var settings = { ip: '0.0.0.0',
  port: 9000,
  lang: 'en',
 inputs: 
   { input0: { def: 53, multi: 40 },
     input1: { def: 55, multi: 40 },
     input2: { def: 62, multi: 40 },
     input3: { def: 45, multi: 45 },
     input4: { def: 55, multi: 40 },
     input5: { def: 65, multi: 45 },
     input6: { def: 60, multi: 45 } },
  app: 
   { debug: false,
     top: '33px',
     left: '132px',
     scale: 1,
     angleX: 1,
     angleY: 0,
     angleZ: 0 },
  logo: { top: '25px', left: '382px' },
  center: 
   { logo: 'myAccount-logo.png',
     title: 'Discover<br>My Account',
     description: 'Discover a few of the tools for controlling and reviewing your Google activity.<br>These help you decide how to make Google services work better for you.',
     top: '379.84020870699493px',
     left: '765.4329039621718px',
     width: '377px',
     scale: 1 },
  close: 
   { description: 'Touch these white fields<br>to find out more<br>about our features.',
     description_active: 'Close this feature.',
     top: '908px',
     left: '1168px',
     width: '157px' },
  features: 
   { feature0: 
      { type: 'ON-OFF-LOOP',
        status: 'disabled',
        initIndex: 0,
        button: { index: 0, time: 0.3, top: '192px', left: '380px', scale: 0.31 },
        copy: 
         { feature: 'Location History',
           sidecopy: 'Please start over to<br>see this feature again.',
           description: 'Creates a private map of where you go with your signed-in devices in order to provide improved map searches, commuting routes and more.',
           description_active: 
            [ 'Creates a private map of where you go with your signed-in devices in order to provide improved map searches, commuting routes and more.',
              'Pause your location tracking and places you go with your devices will stop being added to your LocationHistory map. This limits functionality of some Google products over time, such as Google Maps.',
              'Creating your own map of where you go with your logged-in devices and get for example traffic information for your daily commute.' ],
           top: '194px',
           left: '364px',
           width: '340px',
           scale: 1 },
        video: 
         { center: 
            { top: '300px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 0.95 } },
        lastframes: 
         [ 'LocationHistory/13-04-google-RADIO-LocationHistory-BuildUp.png',
           'LocationHistory/13-04-google-RADIO-LocationHistory_WalkToBar.png',
           'LocationHistory/13-04-google-LocationHistory-WalkHome.png',
           'LocationHistory/13-04-google-LocationHistory-MapFold.png' ],
        videos: 
         [ 'LocationHistory/13-04-google-RADIO-LocationHistory-BuildUp.mov',
           'LocationHistory/13-04-google-RADIO-LocationHistory_WalkToBar.mov',
           'LocationHistory/13-04-google-LocationHistory-WalkHome.mov',
           'LocationHistory/13-04-google-LocationHistory-MapFold.mov' ] },
     feature1: 
      { type: 'ON-OFF',
        button: { index: 1, time: 0.3, top: '97px', left: '802px', scale: 0.31 },
        copy: 
         { feature: 'Voice & Audio Activity',
           description: 'Help recognise your voice and improve speech recognition by storing your voice and audio inputs to your account.',
           description_active: 
            [ 'Help recognise your voice and improve speech recognition by storing your voice and audio inputs to your account.',
              'Pause your Voice & Audio Activity and your voice inputs won’t be saved to your Google Account.',
              'Storing your voice to get you better results and help Google to learn how you say words and phrases.' ],
           top: '103px',
           left: '765.4329039621718px',
           width: '380px',
           scale: 1 },
        video: 
         { center: 
            { top: '300px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 1 } },
        status: 'disabled',
        initIndex: 0,
        lastframes: 
         [ 'AudioSettings/14-04-google-RADIO-AudioSettings-BuildUp.png',
           'AudioSettings/14-04-google-RADIO-AudioSettings-OffLoop.png',
           'AudioSettings/14-04-google-RADIO-AudioSettings-Activation.png',
           'AudioSettings/14-04-google-RADIO-AudioSettings-OnLoop.png' ],
        videos: 
         [ 'AudioSettings/14-04-google-RADIO-AudioSettings-BuildUp.mov',
           'AudioSettings/14-04-google-RADIO-AudioSettings-OffLoop.mov',
           'AudioSettings/14-04-google-RADIO-AudioSettings-Activation.mov',
           'AudioSettings/14-04-google-RADIO-AudioSettings-OnLoop.mov' ] },
     feature2: 
      { type: 'LOOP',
        status: 'enabled',
        initIndex: 0,
        button: { index: 2, time: 0.3,  top: '404px', left: '1219px', scale: 0.31 },
        copy: 
         { feature: 'Ad Settings',
           description: 'Explore how you can control the information that Google uses to show your ads.',
           description_active: 
            [ 'Explore how you can control the information that Google uses to show your ads.',
              'By turning AdSettings on, the ads you see become personalised for you based on previous search queries, for example, or YouTube videos you have watched.',
              'Pause the personalized Ad Settings and you will still see ads, but they will be less relevant. All the advertising interests associated with your Google account will be removed.' ],
           top: '408px',
           left: '1216px',
           width: '313.64487200391324px',
           scale: 1 },
        video: 
         { side: 
             { top: '133px',
              left: '1171px',
              scale: '1.0',
              width: '352px',
              height: '312px' },
           center: 
            { top: '300px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 1 } },
        lastframes: 
         [ 'AdSettings/13-04-google-RADIO-AdSettingsIdle.jpg',
           'AdSettings/13-04-google-RADIO-AdSettingsBuildUp.png',
           'AdSettings/13-04-google-RADIO-AdSettingsIdle-DogAds.png',
           'AdSettings/13-04-google-RADIO-AdSettingsDogAdsToRanAds-Transition.png',
           'AdSettings/13-04-google-RADIO-AdSettingsIdle-RandomAds.png',
           'AdSettings/13-04-google-RADIO-AdSettingsRanAdsToDogAds-Transition.png' ],
        videos: 
         [ 'AdSettings/13-04-google-RADIO-AdSettingsIdle.mov',
           'AdSettings/13-04-google-RADIO-AdSettingsBuildUp.mov',
           'AdSettings/13-04-google-RADIO-AdSettingsIdle-DogAds.mov',
           'AdSettings/13-04-google-RADIO-AdSettingsDogAdsToRanAds-Transition.mov',
           'AdSettings/13-04-google-RADIO-AdSettingsIdle-RandomAds.mov',
           'AdSettings/13-04-google-RADIO-AdSettingsRanAdsToDogAds-Transition.mov' ] },
     feature3: 
      { type: 'LOOP',
        status: 'enabled',
        initIndex: 0,
        button: { index: 3, time: 0.3, top: '674px', left: '407px', scale: 0.31 },
        copy: 
         { feature: 'Web & App Activity',
           description: 'Save your search activity on apps and in browsers to make searches faster and get customised experiences in Search, Maps and other Google products.',
           description_active: 
            [ 'Save your search activity on apps and in browsers to make searches faster and get customised experiences in Search, Maps and other Google products.',
              'Storing your web and app activities can speed up your searches and get customized information.',
              'Pause your Web & App Activity. You can review and delete activities like your searches by visiting \'My Activity\'' ],
           top: '674.2103962435126px',
           left: '384.75134518180334px',
           width: '335px',
           scale: 1 },
        video: 
         { side: 
             { top: '429px',
              left: '365px',
              scale: '1.0',
              width: '352px',
              height: '312px' },
           center: 
            { top: '300px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 1 } },
        lastframes: 
         [ 'WebAppActivity/14-04-google-RADIO-WebActivity-DefaltIdle.png',
           'WebAppActivity/14-04-google-RADIO-WebActivity-BuildUp.png',
           'WebAppActivity/14-04-google-RADIO-WebActivity-CenterIdle.png',
           'WebAppActivity/14-04-google-RADIO-WebActivity-Off(No-Icons).png',
           'WebAppActivity/14-04-google-RADIO-WebActivity-OffIdle.png',
           'WebAppActivity/14-04-google-RADIO-WebActivity-On.png' ],
        videos: 
         [ 'WebAppActivity/14-04-google-RADIO-WebActivity-DefaltIdle.mov',
           'WebAppActivity/14-04-google-RADIO-WebActivity-BuildUp.mov',
           'WebAppActivity/14-04-google-RADIO-WebActivity-CenterIdle.mov',
           'WebAppActivity/14-04-google-RADIO-WebActivity-Off(No-Icons).mov',
           'WebAppActivity/14-04-google-RADIO-WebActivity-OffIdle.mov',
           'WebAppActivity/14-04-google-RADIO-WebActivity-On.mov' ] },
     feature4: 
      { type: 'BUILD-LOOP-OUT',
        playFirstTwo: false,
        status: 'disabled',
        initIndex: 0,
        button: 
         { index: 4,
           time: 0.3,
           top: '844px',
           left: '760px',
           scale: 0.56,
           label: 'CREATE ARCHIVE' },
        copy: 
         { feature: 'Download Your Data',
           description: 'Create an archive with a copy of your data from Goolge products',
           description_active: 
            [ 'Create an archive with a copy of your data from Google products.',
              'Creating a copy of Google data.' ],
           top: '819px',
           left: '815.5225827490624px',
           width: '290px',
           scale: 1 },
        video: 
         { center: 
            { top: '300px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 1 } },
        videos: 
         [ 'DownloadYourData/13-04-google-RADIO-DownloadBuildUp.mov',
           'DownloadYourData/13-04-google-RADIO-DownloadIdle.mov',
           'DownloadYourData/13-04-google-RADIO-DownloadTakeOff.mov' ],
        lastframes: 
         [ 'DownloadYourData/13-04-google-RADIO-DownloadBuildUp.png',
           'DownloadYourData/13-04-google-RADIO-DownloadIdle.png',
           'DownloadYourData/13-04-google-RADIO-DownloadTakeOff.png' ] },
     feature5: 
      { type: 'BUILD-LOOP-OUT',
        playFirstTwo: true,
        status: 'disabled',
        initIndex: 0,
        button: 
         { index: 5,
           time: 0.3,
            top: '656px',
           left: '1164px',
           scale: 0.52,
           label: 'GO TO MY ACTIVITY',
           label_active: 'DELETE' },
        copy: 
         { feature: 'My Activity',
           description: 'Discover and control the data that\'s created when you use Google services.',
           description_active: 
            [ 'Discover and control the data that’s created when you use Google services.',
              'Delete a single item or several ones, or even all activities - as you like.' ],
           top: '630px',
           left: '1216px',
           width: '290px',
           scale: 1 },
        video: 
         { center: 
            { top: '300px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 1 } },
        lastframes: 
         [ 'MyActivity/14-04-google-RADIO-MyActivity-BuildUp.png',
           'MyActivity/14-04-google-RADIO-MyActivity-InspectingLoop.png',
           'MyActivity/14-04-google-RADIO-MyActivity-Erasing.png' ],
        videos: 
         [ 'MyActivity/14-04-google-RADIO-MyActivity-BuildUp.mov',
           'MyActivity/14-04-google-RADIO-MyActivity-InspectingLoop.mov',
           'MyActivity/14-04-google-RADIO-MyActivity-Erasing.mov' ] } } }