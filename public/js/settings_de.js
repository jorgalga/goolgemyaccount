var settings = { ip: '0.0.0.0',
  port: 9000,
  lang: 'de',
  inputs: 
   { input0: { def: 53, multi: 40 },
     input1: { def: 55, multi: 40 },
     input2: { def: 62, multi: 40 },
     input3: { def: 45, multi: 45 },
     input4: { def: 55, multi: 40 },
     input5: { def: 65, multi: 45 },
     input6: { def: 60, multi: 45 } },
  app: 
   { debug: false,
     top: '33px',
     left: '132px',
     scale: 1,
     angleX: 1,
     angleY: 0,
     angleZ: 0 },
  logo: { top: '25px', left: '382px' },
  center: 
   { logo: 'myAccount-logo.png',
     title: 'Entdecke<br>Mein Konto',
     description: 'Entdecke Sie einige unserer Tools, um Ihre Google-Aktivitäten einzusehen und zu steuern.<br>Mithilfe dieser Tools können Sie entscheiden, wie Sie Google-Dienste für sich personalisieren möchten.',
     top: '371.18958502847846px',
     left: '739.879576891782px',
     width: '431.92969894222944px',
     scale: 1 },
  close: 
   { description: 'Berühren Sie die weißen Flächen, um mehr über die Funktionen herauszufinden.',
     description_active: 'Diese Funktion schließen.',
     top: '908px',
     left: '1168px',
     width: '157px' },
  features: 
   { feature0: 
      { type: 'ON-OFF-LOOP',
        status: 'disabled',
        initIndex: 0,
        button: { index: 0, time: 0.3, top: '192px', left: '380px', scale: 0.31 },
        copy: 
         { feature: 'Standortverlauf',
           sidecopy: 'Bitte erneut starten,<br>um diese Funktion noch<br>einmal zu sehen.',
           description: 'Erstellt eine private Karte der Orte, die Sie mit Ihren in Google angemeldeten Geräten besucht haben, um beispielsweise die Kartensuche und Routen für Ihren Arbeitsweg zu verbessern.',
           description_active: 
            [ 'Erstellt eine private Karte der Orte, die Sie mit Ihren in Google angemeldeten Geräten besucht haben, um beispielsweise die Kartensuche und Routen für Ihren Arbeitsweg zu verbessern.',
              'Wenn Sie den Standortverlauf pausieren, werden Orte, die Sie mit Ihren Geräten besuchen, nicht mehr zu Ihrer privaten Karte hinzugefügt. Mit der Zeit kann dies die Funktionalität einiger Google-Produkte wie Google Maps beeinträchtigen.',
              'Erstellt eine private Karte der Orte, die Sie mit Ihren in Google angemeldeten Geräten besucht haben. So erhalten Sie zum Beispiel Verkehrsmeldungen für Ihren täglichen Arbeitsweg.' ],
           top: '194px',
           left: '364px',
           width: '340px',
           scale: 1 },
        video: 
         { center: 
            { top: '330px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 1 } },
        lastframes: 
         [ 'LocationHistory/13-04-google-RADIO-LocationHistory-BuildUp-German.png',
           'LocationHistory/13-04-google-RADIO-LocationHistory_WalkToBar-German.png',
           'LocationHistory/13-04-google-LocationHistory-WalkHome-German.png',
           'LocationHistory/13-04-google-LocationHistory-MapFold-German.png' ],
        videos: 
         [ 'LocationHistory/13-04-google-RADIO-LocationHistory-BuildUp-German.mp4',
           'LocationHistory/13-04-google-RADIO-LocationHistory_WalkToBar-German.mp4',
           'LocationHistory/13-04-google-LocationHistory-WalkHome-German.mp4',
           'LocationHistory/13-04-google-LocationHistory-MapFold-German.mp4' ] },
     feature1: 
      { type: 'ON-OFF',
        button: { index: 1, time: 0.3, top: '97px', left: '802px', scale: 0.31 },
        copy: 
         { feature: 'Sprach- & Audioaktivitäten',
           description: 'Helfen Sie uns, Ihre Stimme besser zu erkennen und die Spracherkennungsfunktion zu optimieren, indem Sie Ihre Sprach- und Audioeingaben in Ihrem Konto speichern.',
           description_active: 
            [ 'Helfen Sie uns, Ihre Stimme besser zu erkennen und die Spracherkennungsfunktion zu optimieren, indem Sie Ihre Sprach- und Audioeingaben in Ihrem Konto speichern.',
              'Pausieren Sie Ihre Sprach- & Audioaktivitäten und Ihre Spracheingaben werden nicht in Ihrem Google-Konto gespeichert.',
              'Helfen Sie Google, den Klang Ihrer Stimme und Ihrer Aussprache zu erlernen, damit Sie schneller relevantere Ergebnisse erhalten.' ],
           top: '103px',
           left: '765.4329039621718px',
           width: '380px',
           scale: 1 },
        video: 
         { center: 
            { top: '330px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 1 } },
        status: 'disabled',
        initIndex: 0,
        lastframes: 
         [ 'AudioSettings/14-04-google-RADIO-AudioSettings-BuildUp-German.png',
           'AudioSettings/14-04-google-RADIO-AudioSettings-OffLoop-German.png',
           'AudioSettings/14-04-google-RADIO-AudioSettings-Activation-German.png',
           'AudioSettings/14-04-google-RADIO-AudioSettings-OnLoop-German.png' ],
        videos: 
         [ 'AudioSettings/14-04-google-RADIO-AudioSettings-BuildUp-German.mp4',
           'AudioSettings/14-04-google-RADIO-AudioSettings-OffLoop-German.mp4',
           'AudioSettings/14-04-google-RADIO-AudioSettings-Activation-German.mp4',
           'AudioSettings/14-04-google-RADIO-AudioSettings-OnLoop-German.mp4' ] },
     feature2: 
      { type: 'LOOP',
        status: 'enabled',
        initIndex: 0,
        button: { index: 2, time: 0.3, top: '404px', left: '1219px', scale: 0.31 },
        copy: 
         { feature: 'Einstellungen für Werbung',
           description: 'Sie können festlegen, welche Informationen Google zur Einblendung von Werbung verwenden darf.',
           description_active: 
            [ 'Sie können festlegen, welche Informationen Google zur Einblendung von Werbung verwenden darf.',
              'Bei Aktivierung der interessenbezogenen Werbung sehen Sie Werbung z. B. auf Basis Ihrer früheren Suchanfragen oder der von Ihnen auf YouTube angesehenen Videos.',
              'Bei einer Deaktivierung sehen Sie weiterhin Werbung, diese ist möglicherweise weniger relevant. Alle mit Ihrem Google-Konto verknüpften Interessen hinsichtlich Werbung werden entfernt.' ],
           top: '408px',
           left: '1215px',
           width: '350px',
           scale: 1 },
        video: 
         { side: 
            { top: '133px',
              left: '1171px',
              scale: '1.0',
              width: '352px',
              height: '312px' },
           center: 
            { top: '330px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 1 } },
        lastframes: 
         [ 'AdSettings/13-04-google-RADIO-AdSettingsIdle-German.png',
           'AdSettings/13-04-google-RADIO-AdSettingsBuildUp-German.png',
           'AdSettings/13-04-google-RADIO-AdSettingsIdle-DogAds-German.png',
           'AdSettings/13-04-google-RADIO-AdSettingsDogAdsToRanAds-Transition-German.png',
           'AdSettings/13-04-google-RADIO-AdSettingsIdle-RandomAds-German.png',
           'AdSettings/13-04-google-RADIO-AdSettingsRanAdsToDogAds-Transition-German.png' ],
        videos: 
         [ 'AdSettings/13-04-google-RADIO-AdSettingsIdle-German.mp4',
           'AdSettings/13-04-google-RADIO-AdSettingsBuildUp-German.mp4',
           'AdSettings/13-04-google-RADIO-AdSettingsIdle-DogAds-German.mp4',
           'AdSettings/13-04-google-RADIO-AdSettingsDogAdsToRanAds-Transition-German.mp4',
           'AdSettings/13-04-google-RADIO-AdSettingsIdle-RandomAds-German.mp4',
           'AdSettings/13-04-google-RADIO-AdSettingsRanAdsToDogAds-Transition-German.mp4' ] },
     feature3: 
      { type: 'LOOP',
        status: 'enabled',
        initIndex: 0,
        button: { index: 3, time: 0.3, top: '674px', left: '407px', scale: 0.31 },
        copy: 
         { feature: 'Web- & App Aktivitäten',
           description: 'Ihre Suchaktivitäten in Apps und Browsern werden gespeichert. So wird die Suche beschleunigt und Sie erhalten personalisierte Informationen in der Suche, Google Maps und anderen Google-Produkten.',
           description_active: 
            [ 'Ihre Suchaktivitäten in Apps und Browsern werden gespeichert. So wird die Suche beschleunigt und Sie erhalten personalisierte Informationen in der Suche, Google Maps und anderen Google-Produkten.',
              'Wenn Sie Ihre Suchaktivitäten in Apps und Browsern speichern, wird die Suche beschleunigt und Sie erhalten personalisierte Informationen - zum Beispiel in der Suche, Google Maps und anderen Google-Produkten.',
              'Pausieren Sie Ihre Web- & App-Aktivitäten. Unter „Meine Aktivitäten“ können Sie Ihre Aktivitäten wie zum Beispiel Ihre Suchanfragen ansehen und löschen.' ],
           top: '678px',
           left: '386px',
           width: '335px',
           scale: 1 },
        video: 
         { side: 
            { top: '429px',
              left: '365px',
              scale: '1.0',
              width: '352px',
              height: '312px' },
           center: 
            { top: '330px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 1 } },
        lastframes: 
         [ 'WebAppActivity/14-04-google-RADIO-WebActivity-DefaltIdle-German.png',
           'WebAppActivity/14-04-google-RADIO-WebActivity-BuildUp-German.png',
           'WebAppActivity/14-04-google-RADIO-WebActivity-CenterIdle-German.png',
           'WebAppActivity/14-04-google-RADIO-WebActivity-Off(No-Icons)-German.png',
           'WebAppActivity/14-04-google-RADIO-WebActivity-OffIdle-German.png',
           'WebAppActivity/14-04-google-RADIO-WebActivity-On-German.png' ],
        videos: 
         [ 'WebAppActivity/14-04-google-RADIO-WebActivity-DefaltIdle-German.mp4',
           'WebAppActivity/14-04-google-RADIO-WebActivity-BuildUp-German.mp4',
           'WebAppActivity/14-04-google-RADIO-WebActivity-CenterIdle-German.mp4',
           'WebAppActivity/14-04-google-RADIO-WebActivity-Off(No-Icons)-German.mp4',
           'WebAppActivity/14-04-google-RADIO-WebActivity-OffIdle-German.mp4',
           'WebAppActivity/14-04-google-RADIO-WebActivity-On-German.mp4' ] },
     feature4: 
      { type: 'BUILD-LOOP-OUT',
        playFirstTwo: false,
        status: 'disabled',
        initIndex: 0,
        button: 
         { index: 4,
           time: 0.3,
           top: '844px',
           left: '760px',
           scale: 0.56,
           label: 'Archiv erstellen' },
        copy: 
         { feature: 'Daten herunterladen',
           description: 'Erstellen Sie ein Archiv mit einer Kopie Ihrer Daten aus Google-Produkten.',
           description_active: 
            [ 'Erstellen Sie ein Archiv mit einer Kopie Ihrer Daten aus Google-Produkten.',
              'Erstellen Sie eine Kopie Ihrer Google-Daten.' ],
           top: '819px',
           left: '815.5225827490624px',
           width: '290px',
           scale: 1 },
        video: 
         { center: 
            { top: '330px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 1 } },
        videos: 
         [ 'DownloadYourData/13-04-google-RADIO-DownloadBuildUp-German.mp4',
           'DownloadYourData/13-04-google-RADIO-DownloadIdle-German.mp4',
           'DownloadYourData/13-04-google-RADIO-DownloadTakeOff-German.mp4' ],
        lastframes: 
         [ 'DownloadYourData/13-04-google-RADIO-DownloadBuildUp-German.png',
           'DownloadYourData/13-04-google-RADIO-DownloadIdle-German.png',
           'DownloadYourData/13-04-google-RADIO-DownloadTakeOff-German.png' ] },
     feature5: 
      { type: 'BUILD-LOOP-OUT',
        playFirstTwo: true,
        status: 'disabled',
        initIndex: 0,
        button: 
         { index: 5,
           time: 0.3,
           top: '656px',
           left: '1164px',
           scale: 0.52,
           label: 'GEHE ZU Meine Aktivitäten',
           label_active: 'Löschen' },
        copy: 
         { feature: 'Meine Aktivitäten',
           description: 'Hier können Sie sehen und steuern, welche Daten bei der Verwendung von Google-Diensten erzeugt werden.',
           description_active: 
            [ 'Hier können Sie sehen und steuern, welche Daten bei der Verwendung von Google-Diensten erzeugt werden.',
              'Löschen Sie einzelne oder mehrere Elemente, oder auch alle Aktivitäten - wie Sie wollen.' ],
           top: '630px',
           left: '1216px',
           width: '290px',
           scale: 1 },
        video: 
         { center: 
            { top: '330px',
              left: '675px',
              width: '540px',
              height: '480px',
              scale: 1 } },
        lastframes: 
         [ 'MyActivity/14-04-google-RADIO-MyActivity-BuildUp-German.png',
           'MyActivity/14-04-google-RADIO-MyActivity-InspectingLoop-German.png',
           'MyActivity/14-04-google-RADIO-MyActivity-Erasing-German.png' ],
        videos: 
         [ 'MyActivity/14-04-google-RADIO-MyActivity-BuildUp-German.mp4',
           'MyActivity/14-04-google-RADIO-MyActivity-InspectingLoop-German.mp4',
           'MyActivity/14-04-google-RADIO-MyActivity-Erasing-German.mp4' ] } } }