module.exports = {
    port: 9000,
    center: {
        logo: "google_logo1600.png",
        title: "Discover<br>Google<br>My Account",
        description: "Discover a few of the tools for controlling and reviewing your Google activity.<br>These help you decide how to make Google services work better for you",
        top: "33%",
        left: "50%",
        width: "20%",
    },
    close: {
        description: "Touch these white fields to find out more about our features",
        description_active: "Close this feature",
        top: "78.5%",
        left: "63%",
        width: "10%"
    },
    features: {
        feature0: {
            button: {
                index: 0,
                time: 0.3,
                top: "20.5%",
                left: "21.5%",
                scale: 0.3
            },
            copy: {
                feature: "Location History",
                description: "Creates a private map of where you go with your signed-in devices in order to provide improved map searches, commuting routes and more.",
                description_active: [
                    "Pause your location tracking and places you go with your devices will stop being added to your Location History map. This limits functionality of some Google products over time, such as Google Maps.",
                    "Creating your own map of where you go with your logged-in devices and get for example traffic information for your daily commute.",
                    "Pause your location tracking and places you go with your devices will stop being added to your Location History map. This limits functionality of some Google products over time, such as Google Maps."
                ],
                top: "21%",
                left: "29%",
                width: "20%",
                scale: 1
            },
            video: {
                center: {
                    top: "28%",
                    left: "38%",
                    width: "23%",
                }
            },
            status: "disabled",
            initIndex: 0,
            lastframes: [
                "lastframe_on.png",
                "lastframe_off.png"
            ],
            videos: [
                "Google_ActivityControls_ON_b.mp4",
                "Google_ActivityControls_OFF_b.mp4"
            ]
        },
        feature1: {
            button: {
                index: 1,
                time: 0.3,
                top: "14.5%",
                left: "42.5%",
                scale: 0.3
            },
            copy: {
                feature: "Voice + Audio Activity",
                description: "Help recognise your voice and improve speech recognition by storing your voice and audio inputs to your account.",
                description_active: "[Copy Active]",
                top: "14%",
                left: "50%",
                width: "20%",
                scale: 1
            },
            video: {
                center: {
                    top: "28%",
                    left: "38%",
                    width: "23%"
                }
            },
            status: "disabled",
            initIndex: 0,
            lastframes: [
                "lastframe_on.png",
                "lastframe_off.png"
            ],
            videos: [
                "Google_ActivityControls_ON_b.mp4",
                "Google_ActivityControls_OFF_b.mp4"
            ]
        },
        feature2: {
            button: {
                index: 2,
                time: 0.3,
                top: "36.5%",
                left: "62%",
                scale: 0.3
            },
            copy: {
                feature: "Ad settings",
                description: "Explore how you can control the information that Google uses to show your ads.",
                description_active: "[Copy Active]",
                top: "37%",
                left: "71%",
                width: "14%",
                scale: 1
            },
            video: {
                side: {
                    top: "14%",
                    left: "60%",
                    width: "22%"
                },
                center: {
                    top: "28%",
                    left: "38%",
                    width: "23%"
                }
            },
            status: "disabled",
            initIndex: 0,
            lastframes: [
                "lastframe_on.png",
                "lastframe_off.png"
            ],
            videos: [
                "Google_ActivityControls_ON_b.mp4",
                "Google_ActivityControls_OFF_b.mp4"
            ]
        },
        feature3: {
            button: {
                index: 3,
                time: 0.3,
                top: "60.5%",
                left: "21.5%",
                scale: 0.3
            },
            copy: {
                feature: "Web + App Activity",
                description: "Save your search activity on apps and in browsers to make searches faster and get customised experiences in Search, Maps and other Google products.",
                description_active: "[Copy Active]",
                top: "61%",
                left: "29%",
                width: "18%",
                scale: 1
            },
            video: {
                side: {
                    top: "40%",
                    left: "18%",
                    width: "20%"
                },
                center: {
                    top: "28%",
                    left: "38%",
                    width: "23%"
                }
            },
            status: "disabled",
            initIndex: 0,
            lastframes: [
                "lastframe_on.png",
                "lastframe_off.png"
            ],
            videos: [
                "Google_ActivityControls_ON_b.mp4",
                "Google_ActivityControls_OFF_b.mp4"
            ]
        },
        feature4: {
            button: {
                index: 4,
                time: 0.3,
                top: "73%",
                left: "41.5%",
                scale: 0.55,
                label: "CREATE ARCHIVE"
            },
            copy: {
                feature: "Download your Data",
                description: "Create an archive with a copy of your data from Goolge products",
                description_active: "[Copy Active]",
                top: "70%",
                left: "50%",
                width: "16%",
                scale: 1
            },
            video: {
                center: {
                    top: "28%",
                    left: "38%",
                    width: "23%"
                }
            },
            status: "disabled",
            initIndex: 0,
            lastframes: [
                "lastframe_on.png",
                "lastframe_off.png"
            ],
            videos: [
                "Google_ActivityControls_ON_b.mp4",
                "Google_ActivityControls_OFF_b.mp4"
            ]
        },
        feature5: {
            button: {
                index: 5,
                time: 0.3,
                top: "61%",
                left: "60.75%",
                scale: 0.55,
                label: "CREATE ARCHIVE"
            },
            copy: {
                feature: "My Activity",
                description: "Discover and control the data that's created when you use Google services.",
                description_active: "[Copy Active]",
                top: "58%",
                left: "70%",
                width: "18%",
                scale: 1
            },
            video: {
                center: {
                    top: "28%",
                    left: "38%",
                    width: "23%"
                }
            },
            status: "disabled",
            initIndex: 0,
            lastframes: [
                "lastframe_on.png",
                "lastframe_off.png"
            ],
            videos: [
                "Google_ActivityControls_ON_b.mp4",
                "Google_ActivityControls_OFF_b.mp4"
            ]
        }
    }
}
