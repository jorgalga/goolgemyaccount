var createjs = require('preload-js');
module.exports = Loader = function (features, lang) {
    this.files = [];
    var el = {};
    for (var key in features) {
        el = features[key];
        for(i=0; i < el.lastframes.length; i++) {
            this.files.push("../videos/"+lang+"/" + el.lastframes[i]);
        }
        for(i=0; i < el.videos.length; i++) {
            this.files.push("../videos/"+lang+"/" + el.videos[i]);
        }
    }
}

Loader.prototype.loadFiles = function(callback){
    var _self = this;
    var a_loaded = 0;
    for(i=0; i < this.files.length; i++) {
        loadAsset(this.files[i]);
    }

    function loadAsset(file) {
        var preload = new createjs.LoadQueue();
        preload.addEventListener("fileload", handleFileComplete);
        preload.loadFile(file);
    }

    function handleFileComplete() {
        a_loaded ++;
        if(a_loaded == _self.files.length) {
            callback("All the assets loaded");
        }
    }
}
