//var settings = require("../settings/settings.js");
var ScreenManager = require("../utils/ScreenManager");
var Videoblock = require("../videoblock/Videoblock");
var Centerblock = require("../centerblock/Centerblock");
var Closebutton = require("../closebutton/Closebutton");
var Loader = require("../loader/Loader");
var SocketManager = require("../socketManager/SocketManager");

// LOADER
var asset_loader = new Loader(settings.features, settings.lang);
// APP DOM element
var app_dom = document.getElementById("app");
app_dom.style.top = settings.app.top;
app_dom.style.left = settings.app.left;
app_dom.style.transform = "scale3d("+settings.app.scale+","+settings.app.scale+","+settings.app.scale+") rotateX("+settings.app.angleX+"deg) rotateY("+settings.app.angleY+"deg) rotateZ("+settings.app.angleZ+"deg)";

// SOCKET.IO URL
var socket = io('http://'+settings.ip+':'+settings.port);

// SCREEN MANAGER
var screen_manager = new ScreenManager(document.documentElement);

// GOOGLE LOGO
var google_logo = document.getElementById("google_logo");
google_logo.style.top = settings.logo.top;
google_logo.style.left = settings.logo.left;

// CENTER EL
var center = new Centerblock(app_dom, settings.center);

// CLOSE BUTTON
var close_btn = new Closebutton(app_dom, settings.close);

// 6 FEATURES
var video_block = {};
var video_blocks_array = [];
for (var key in settings.features) {
    if (settings.features.hasOwnProperty(key)) {
        video_block = new Videoblock(app_dom, settings.features[key], settings.lang);
        video_block.createDomElements();
        video_blocks_array.push(video_block);
    }
}
for (i=0; i < video_blocks_array.length; i++){
    video_blocks_array[i].button_el.reScaleToggle();
}

// FADE SEQUENCE
setTimeout(function(){
    asset_loader.loadFiles(function(result){
        document.getElementById("loader").classList.add("hidden");
        setTimeout(function(){
            document.getElementById("app").classList.add("visible");
            document.body.style.perspective = "800px";
            //GO TO FULLSCREEN
            screen_manager.toggle();
        }, 1000);
    });
}, 2000);

// SOCKETMANAGER
var socket_manager = new SocketManager(settings.app.dedug, socket, video_blocks_array, center, close_btn, settings.inputs);

// ADJUST GUI
buildGUI();

// DEBUG LAYOUT IF IS ACTIVE
debugLayout(settings.app.debug)

function buildGUI(){
    // GUI
    var max_top = 1080;
    var max_left = 1920;
    var max_width = 512;
    var max_scale = 2.0;
    var controllers = [];
    var controllers_input = [];
    var datGUI = function() {
        // APP
        var apps = settings.app;
        this.app__debug = apps.debug;
        this.app__top = parseInt(apps.top.replace("px",""));
        this.app__left = parseInt(apps.left.replace("px",""));
        this.app__scale = apps.scale;
        this.app__angleX = apps.angleX;
        this.app__angleY = apps.angleY;
        this.app__angleZ = apps.angleZ;
        //CENTER
        var centers = settings.center;
        this.center_container__top = parseInt(centers.top.replace("px",""));
        this.center_container__left = parseInt(centers.left.replace("px",""));
        this.center_container__width = parseInt(centers.width.replace("px",""));
        this.center_container__scale = centers.scale;

        //GOOLGE LOGO
        var logos = settings.logo;
        this.google_logo__top = parseInt(logos.top.replace("px",""));
        this.google_logo__left = parseInt(logos.left.replace("px",""));

        //CLOSE BUTTON
        var closes = settings.close;
        this.close_container__top = parseInt(closes.top.replace("px",""));
        this.close_container__left = parseInt(closes.left.replace("px",""));

        // FEATURE 0
        var buttons = settings.features.feature0.button;
        this.button_wrapper_0__top = parseInt(buttons.top.replace("px",""));
        this.button_wrapper_0__left = parseInt(buttons.left.replace("px",""));
        this.button_wrapper_0__scale = buttons.scale;

        var copys = settings.features.feature0.copy;
        this.text_button_container_0__top = parseInt(copys.top.replace("px",""));
        this.text_button_container_0__left = parseInt(copys.left.replace("px",""));
        this.text_button_container_0__width = parseInt(copys.width.replace("px",""));
        this.text_button_container_0__scale = copys.scale;

        // FEATURE 1
        buttons = settings.features.feature1.button;
        this.button_wrapper_1__top = parseInt(buttons.top.replace("px",""));
        this.button_wrapper_1__left = parseInt(buttons.left.replace("px",""));
        this.button_wrapper_1__scale = buttons.scale;

        copys = settings.features.feature1.copy;
        this.text_button_container_1__top = parseInt(copys.top.replace("px",""));
        this.text_button_container_1__left = parseInt(copys.left.replace("px",""));
        this.text_button_container_1__width = parseInt(copys.width.replace("px",""));
        this.text_button_container_1__scale = copys.scale;

        // FEATURE 2
        buttons = settings.features.feature2.button;
        this.button_wrapper_2__top = parseInt(buttons.top.replace("px",""));
        this.button_wrapper_2__left = parseInt(buttons.left.replace("px",""));
        this.button_wrapper_2__scale = buttons.scale;

        copys = settings.features.feature2.copy;
        this.text_button_container_2__top = parseInt(copys.top.replace("px",""));
        this.text_button_container_2__left = parseInt(copys.left.replace("px",""));
        this.text_button_container_2__width = parseInt(copys.width.replace("px",""));
        this.text_button_container_2__scale = copys.scale;

        // FEATURE 3
        buttons = settings.features.feature3.button;
        this.button_wrapper_3__top = parseInt(buttons.top.replace("px",""));
        this.button_wrapper_3__left = parseInt(buttons.left.replace("px",""));
        this.button_wrapper_3__scale = buttons.scale;

        copys = settings.features.feature3.copy;
        this.text_button_container_3__top = parseInt(copys.top.replace("px",""));
        this.text_button_container_3__left = parseInt(copys.left.replace("px",""));
        this.text_button_container_3__width = parseInt(copys.width.replace("px",""));
        this.text_button_container_3__scale = copys.scale;

        // FEATURE 4
        buttons = settings.features.feature4.button;
        this.button_wrapper_4__top = parseInt(buttons.top.replace("px",""));
        this.button_wrapper_4__left = parseInt(buttons.left.replace("px",""));
        this.button_wrapper_4__scale = buttons.scale;

        copys = settings.features.feature4.copy;
        this.text_button_container_4__top = parseInt(copys.top.replace("px",""));
        this.text_button_container_4__left = parseInt(copys.left.replace("px",""));
        this.text_button_container_4__width = parseInt(copys.width.replace("px",""));
        this.text_button_container_4__scale = copys.scale;

        // FEATURE 5
        buttons = settings.features.feature5.button;
        this.button_wrapper_5__top = parseInt(buttons.top.replace("px",""));
        this.button_wrapper_5__left = parseInt(buttons.left.replace("px",""));
        this.button_wrapper_5__scale = buttons.scale;

        copys = settings.features.feature5.copy;
        this.text_button_container_5__top = parseInt(copys.top.replace("px",""));
        this.text_button_container_5__left = parseInt(copys.left.replace("px",""));
        this.text_button_container_5__width = parseInt(copys.width.replace("px",""));
        this.text_button_container_5__scale = copys.scale;

        // INPUT 0
        var inputs = settings.inputs.input0;
        this.input0__def =  inputs.def;
        this.input0__multi =  inputs.multi;
        // INPUT 1
        inputs = settings.inputs.input1;
        this.input1__def =  inputs.def;
        this.input1__multi =  inputs.multi;
        // INPUT 2
        inputs = settings.inputs.input2;
        this.input2__def =  inputs.def;
        this.input2__multi =  inputs.multi;
        // INPUT 3
        inputs = settings.inputs.input3;
        this.input3__def =  inputs.def;
        this.input3__multi =  inputs.multi;
        // INPUT 4
        inputs = settings.inputs.input4;
        this.input4__def =  inputs.def;
        this.input4__multi =  inputs.multi;
        // INPUT 5
        inputs = settings.inputs.input5;
        this.input5__def =  inputs.def;
        this.input5__multi =  inputs.multi;
        // INPUT 6
        inputs = settings.inputs.input6;
        this.input6__def =  inputs.def;
        this.input6__multi =  inputs.multi;

        //SAVE
        this.save = function() {
            socket.emit("savefile", settings);
            alert("Options SAVED in settings.js ")
        };
    };

    var text = new datGUI();
    var gui = new dat.GUI({ width: 500 });
    gui.domElement.id = 'gui';
    dat.GUI.toggleHide();

    var gui_app = gui.addFolder("App");
    var gui_logo = gui.addFolder("Logo");
    var gui_center = gui.addFolder("Center");
    var gui_close = gui.addFolder("Close button");
    var gui_btn0 = gui.addFolder("F0 Location History");
    var gui_btn1 = gui.addFolder("F1 Voice & Audio Activity");
    var gui_btn2 = gui.addFolder("F2 Ad Settings");
    var gui_btn3 = gui.addFolder("F3 Web & App Activity");
    var gui_btn4 = gui.addFolder("F4 Download Your Data");
    var gui_btn5 = gui.addFolder("F5 My Activity");
    var gui_inputs = gui.addFolder("Inputs");

    // APP
    controllers.push(gui_app.add(text, "app__debug"));
    controllers.push(gui_app.add(text, "app__top", -max_top, max_top));
    controllers.push(gui_app.add(text, "app__left", -max_left, max_left));
    controllers.push(gui_app.add(text, "app__scale", 0.0, max_scale));

    controllers.push(gui_app.add(text, "app__angleX", -30.0, 30.0));
    controllers.push(gui_app.add(text, "app__angleY", -30.0, 30.0));
    controllers.push(gui_app.add(text, "app__angleZ", -30.0, 30.0));

    // LOGO
    controllers.push(gui_logo.add(text, "google_logo__top", -max_top, max_top));
    controllers.push(gui_logo.add(text, "google_logo__left", -max_left, max_left));

    // CENTER
    controllers.push(gui_center.add(text, "center_container__top",0, max_top));
    controllers.push(gui_center.add(text, "center_container__left",0, max_left));
    controllers.push(gui_center.add(text, "center_container__width",0, max_width));
    controllers.push(gui_center.add(text, "center_container__scale",0.0, max_scale));

    // CLOSE
    controllers.push(gui_close.add(text, "close_container__top", 0, max_top));
    controllers.push(gui_close.add(text, "close_container__left", 0, max_left));

    // BUTTON 0
    controllers.push(gui_btn0.add(text, "button_wrapper_0__top",0, max_top));
    controllers.push(gui_btn0.add(text, "button_wrapper_0__left",0, max_left));
    controllers.push(gui_btn0.add(text, "button_wrapper_0__scale",0.0, max_scale));

    controllers.push(gui_btn0.add(text, "text_button_container_0__top",0, max_top));
    controllers.push(gui_btn0.add(text, "text_button_container_0__left",0, max_left));
    controllers.push(gui_btn0.add(text, "text_button_container_0__width",0, max_width));
    controllers.push(gui_btn0.add(text, "text_button_container_0__scale",0.0, max_scale));

    // BUTTON 1
    controllers.push(gui_btn1.add(text, "button_wrapper_1__top",0, max_top));
    controllers.push(gui_btn1.add(text, "button_wrapper_1__left",0, max_left));
    controllers.push(gui_btn1.add(text, "button_wrapper_1__scale",0.0, max_scale));

    controllers.push(gui_btn1.add(text, "text_button_container_1__top",0, max_top));
    controllers.push(gui_btn1.add(text, "text_button_container_1__left",0, max_left));
    controllers.push(gui_btn1.add(text, "text_button_container_1__width",0, max_width));
    controllers.push(gui_btn1.add(text, "text_button_container_1__scale",0.0, max_scale));

    // BUTTON 2
    controllers.push(gui_btn2.add(text, "button_wrapper_2__top",0, max_top));
    controllers.push(gui_btn2.add(text, "button_wrapper_2__left",0, max_left));
    controllers.push(gui_btn2.add(text, "button_wrapper_2__scale",0.0, max_scale));

    controllers.push(gui_btn2.add(text, "text_button_container_2__top",0, max_top));
    controllers.push(gui_btn2.add(text, "text_button_container_2__left",0, max_left));
    controllers.push(gui_btn2.add(text, "text_button_container_2__width",0, max_width));
    controllers.push(gui_btn2.add(text, "text_button_container_2__scale",0.0, max_scale));

    // BUTTON 3
    controllers.push(gui_btn3.add(text, "button_wrapper_3__top",0, max_top));
    controllers.push(gui_btn3.add(text, "button_wrapper_3__left",0, max_left));
    controllers.push(gui_btn3.add(text, "button_wrapper_3__scale",0.0, max_scale));

    controllers.push(gui_btn3.add(text, "text_button_container_3__top",0, max_top));
    controllers.push(gui_btn3.add(text, "text_button_container_3__left",0, max_left));
    controllers.push(gui_btn3.add(text, "text_button_container_3__width",0, max_width));
    controllers.push(gui_btn3.add(text, "text_button_container_3__scale",0.0, max_scale));

    // BUTTON 4
    controllers.push(gui_btn4.add(text, "button_wrapper_4__top",0, max_top));
    controllers.push(gui_btn4.add(text, "button_wrapper_4__left",0, max_left));
    controllers.push(gui_btn4.add(text, "button_wrapper_4__scale",0.0, max_scale));

    controllers.push(gui_btn4.add(text, "text_button_container_4__top",0, max_top));
    controllers.push(gui_btn4.add(text, "text_button_container_4__left",0, max_left));
    controllers.push(gui_btn4.add(text, "text_button_container_4__width",0, max_width));
    controllers.push(gui_btn4.add(text, "text_button_container_4__scale",0.0, max_scale));

    // BUTTON 5
    controllers.push(gui_btn5.add(text, "button_wrapper_5__top",0, max_top));
    controllers.push(gui_btn5.add(text, "button_wrapper_5__left",0, max_left));
    controllers.push(gui_btn5.add(text, "button_wrapper_5__scale",0.0, 1.0));

    controllers.push(gui_btn5.add(text, "text_button_container_5__top",0, max_top));
    controllers.push(gui_btn5.add(text, "text_button_container_5__left",0, max_left));
    controllers.push(gui_btn5.add(text, "text_button_container_5__width",0, max_width));
    controllers.push(gui_btn5.add(text, "text_button_container_5__scale",0.0, max_scale));

    //inputs
    controllers_input.push(gui_inputs.add(text, "input0__def", 0, 255));
    controllers_input.push(gui_inputs.add(text, "input0__multi", 0, 50));

    controllers_input.push(gui_inputs.add(text, "input1__def", 0, 255));
    controllers_input.push(gui_inputs.add(text, "input1__multi", 0, 50));

    controllers_input.push(gui_inputs.add(text, "input2__def", 0, 255));
    controllers_input.push(gui_inputs.add(text, "input2__multi", 0, 50));

    controllers_input.push(gui_inputs.add(text, "input3__def", 0, 255));
    controllers_input.push(gui_inputs.add(text, "input3__multi", 0, 50));

    controllers_input.push(gui_inputs.add(text, "input4__def", 0, 255));
    controllers_input.push(gui_inputs.add(text, "input4__multi", 0, 50));

    controllers_input.push(gui_inputs.add(text, "input5__def", 0, 255));
    controllers_input.push(gui_inputs.add(text, "input5__multi", 0, 50));

    controllers_input.push(gui_inputs.add(text, "input6__def", 0, 255));
    controllers_input.push(gui_inputs.add(text, "input6__multi", 0, 50));

    for(i=0; i< controllers.length; i++){
        controllers[i].onChange(function(value){

            var el = document.getElementById(this.property.split("__")[0]);
            var prop = this.property.split("__")[1];

            var sets = getSettings(this.property.split("__")[0], prop, value);
            updateStyle(el, sets);

        });
    }

    for(let i = 0; i < controllers_input.length; i++){
        controllers_input[i].onChange(function(value){
            if(this.property.split("__")[1] == 'def'){
                socket_manager.def[i/2] = value;
                settings.inputs["input"+(i/2)].def = value;

            }
            else if(this.property.split("__")[1] == 'multi'){
                socket_manager.multi[(i-1)/2.0] = value;
                settings.inputs["input"+(i-1)/2.0+""].multi = value;
            }
        });
    }

    var save = gui.addFolder("SAVE");
    save.add(text, 'save');
}



function getSettings(name, prop, value){
    var sets = {};
    if(name === "app" ) { sets = settings.app; }
    else if(name === "google_logo") { sets = settings.logo;}
    else if(name === "center_container") { sets = settings.center;}
    else if(name === "close_container") { sets = settings.close;}
    else if(name === "button_wrapper_0") { sets = settings.features.feature0.button;}
    else if(name === "button_wrapper_1") { sets = settings.features.feature1.button;}
    else if(name === "button_wrapper_2") { sets = settings.features.feature2.button;}
    else if(name === "button_wrapper_3") { sets = settings.features.feature3.button;}
    else if(name === "button_wrapper_4") { sets = settings.features.feature4.button;}
    else if(name === "button_wrapper_5") { sets = settings.features.feature5.button;}
    else if(name === "text_button_container_0") { sets = settings.features.feature0.copy;}
    else if(name === "text_button_container_1") { sets = settings.features.feature1.copy;}
    else if(name === "text_button_container_2") { sets = settings.features.feature2.copy;}
    else if(name === "text_button_container_3") { sets = settings.features.feature3.copy;}
    else if(name === "text_button_container_4") { sets = settings.features.feature4.copy;}
    else if(name === "text_button_container_5") { sets = settings.features.feature5.copy;}
    else if(name === "input0") { sets = settings.inputs.input0;}
    else if(name === "input1") { sets = settings.inputs.input1;}
    else if(name === "input2") { sets = settings.inputs.input2;}
    else if(name === "input3") { sets = settings.inputs.input3;}
    else if(name === "input4") { sets = settings.inputs.input4;}
    else if(name === "input5") { sets = settings.inputs.input5;}
    else if(name === "input6") { sets = settings.inputs.input6;}

    switch (prop) {
      case "debug":
        sets.debug = value;
        break;
      case "top":
        sets.top = value + "px";
        break;
      case "left":
        sets.left = value + "px";
        break;
      case "width":
        sets.width = value + "px";
        break;
      case "scale":
        sets.scale = value;
        break;
      case "angleX":
        sets.angleX = value;
        break;
      case "angleY":
        sets.angleY = value;
        break;
      case "angleZ":
        sets.angleZ = value;
        break;
      case "def":
        sets.def = value;
        break;
      case "multi":
        sets.multi = value;
        break;
      default:
    }
    return sets;
}

function updateStyle(el, settings){

    if(settings.debug !== undefined){
        debugLayout(settings.debug);
    }

    if(settings.top) el.style.top = settings.top;
    if(settings.left) el.style.left = settings.left;
    if(settings.width){ el.style.width = settings.width;}

    if(settings.angleX || settings.angleY || settings.angleZ){
      el.style.transform = "scale3d("+settings.scale+","+settings.scale+","+settings.scale+") rotateX("+settings.angleX+"deg) rotateY("+settings.angleY+"deg) rotateZ("+settings.angleZ+"deg)";
    }
    else if (settings.scale) {el.style.transform = "scale("+settings.scale+")";}
}

function debugLayout(on){
    if(on){
        document.getElementById("mycanvas").style.opacity = 0.8;
        document.getElementById("mycanvas").style.height = "100px";
        document.getElementById("layout").style.opacity = 0.8;
        app_dom.style.outline = "green solid 6px";
        document.getElementById("gui").style.marginTop = "0px";
    }
    else{
        document.getElementById("mycanvas").style.opacity = 0;
        document.getElementById("mycanvas").style.height = "0px";
        document.getElementById("layout").style.opacity = 0;
        app_dom.style.outline = "none";
        document.getElementById("gui").style.marginTop = "-100px";
    }
}
