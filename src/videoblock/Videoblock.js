/** main class representing a Feature. It is a wrapper for the Video, text and button elements.*/
var cb = require("../cbutton/Cbutton");
module.exports = Videoblock = function (app_dom, settings, lang) {
    this.app_dom = app_dom;
    this.settings = settings;
    this.lang = lang;

    this.on_center = false;
    this.firstplayback = true;
    this.on_to_off = false;
    this.videoLoad = false;
    this.ctimeOut = {};

    this.v_index = this.settings.initIndex;

    this.video_container_el = {};
    this.video_el = {};
    this.button_el = {};
}

/**
 * It returns the description of the Video Block feature plus the index
 *
 * @returns {String}
 */
Videoblock.prototype.toString = function () {
    return "This object is a Videoblock with the button " + this.settings.button.index;
}

/**
 * It sets the video url
 *
 *  @param {string} vurls the path of the video.
 */
Videoblock.prototype.setVideoURLs = function (vurls) {
    this.videoURLs = vurls;
}

/**
 * Creates the DOM elements, it applies the css that is in the settings and it appends to the app element.
 *
 */
Videoblock.prototype.createDomElements = function () {
    this.video_container_el = document.createElement("DIV");
    this.video_container_el.setAttribute("id", "video_container_" + this.settings.button.index);
    this.video_container_el.classList.add("video-container");

    switch (this.settings.type) {
        case "BUILD-LOOP-OUT":
            this.video_container_el.style.backgroundImage = "url('../videos/" + this.lang + "/" + this.settings.lastframes[this.v_index];
            break;
        case "ON-OFF-LOOP":
            this.video_container_el.style.backgroundImage = "url('../videos/" + this.lang + "/" + this.settings.lastframes[this.v_index];
            break;
        case "ON-OFF":
            this.video_container_el.style.backgroundImage = "url('../videos/" + this.lang + "/" + this.settings.lastframes[this.v_index];
            break;
        case "LOOP":
            this.video_container_el.style.backgroundImage = "url('../videos/" + this.lang + "/" + this.settings.lastframes[this.v_index];
            break;
        default:
    }

    if (this.settings.video.side) {
        this.video_container_el.style.top = this.settings.video.side.top;
        this.video_container_el.style.left = this.settings.video.side.left;
        this.video_container_el.style.width = this.settings.video.side.width;
        this.video_container_el.style.height = this.settings.video.side.height;
        this.video_container_el.style.backgroundSize = this.settings.video.side.width + " " + this.settings.video.side.height;
    } else {
        this.video_container_el.style.top = "0";
        this.video_container_el.style.left = "1920px"
        this.video_container_el.style.width = this.settings.video.center.width;
        this.video_container_el.style.height = this.settings.video.center.height;
        this.video_container_el.style.backgroundSize = this.settings.video.center.width + " " + this.settings.video.center.height;
    }
    if (this.settings.video.center.scale) {
        this.video_container_el.style.transform = "scale(" + this.settings.video.center.scale + ")";
    }

    this.video_container_el.style.zIndex = 400 + this.settings.button.index;
    this.video_el = document.createElement("VIDEO");
    this.video_el.setAttribute("id", "video-" + this.settings.button.index);
    this.video_el.setAttribute("src", "../videos/" + this.lang + "/" + this.settings.videos[this.v_index]);
    this.video_el.classList.add("video");

    this.video_container_el.appendChild(this.video_el);

    this.button_el = new Cbutton(this.app_dom, this.settings);

    this.app_dom.appendChild(this.video_container_el);

    // LISTENERS
    switch (this.settings.type) {
        case "BUILD-LOOP-OUT":
            this.video_el.addEventListener('ended', this.onVideoEnded_BLP.bind(this));
            break;
        case "ON-OFF-LOOP":
            this.video_el.addEventListener('ended', this.onVideoEnded_ONOFFLOOP.bind(this));
            break;
        case "ON-OFF":
            this.video_el.addEventListener('ended', this.onVideoEnded_ONOFF.bind(this));
            break;
        case "LOOP":
            var _self = this;
            this.video_el.addEventListener('loadeddata', function(){
                if(_self.videoLoad){
                    _self.videoLoad = false;
                     setTimeout(function () {
                        _self.playVideo();
                    }, 500);
                }
            });
            this.video_el.addEventListener('ended', this.onVideoEnded_LOOP.bind(this));
            this.playVideo();
            break;
        default:
    }

    var _self = this;
    this.video_el.addEventListener('timeupdate', function () {
        _self.button_el.setProgress(Math.trunc(this.currentTime / this.duration * 100));
    });
}

/**
 * It plays the propper video taking care of every kind of feature.
 *
 *  @param {boolean} btn_feedback Indicates that the playback needs a visual feedback for the button
 */
Videoblock.prototype.playVideo = function (btn_feedback) {
    switch (this.settings.type) {
        case "BUILD-LOOP-OUT":
            this.video_el.play();

            if (this.v_index === 1 || this.v_index === 2) {
                this.disableButton();
            }
            var _self = this;
            setTimeout(function () {
                // Adding lastframe image to the background container
                _self.video_container_el.style.backgroundImage = "url('../videos/" + _self.lang + "/" + _self.settings.lastframes[_self.v_index];
                _self.video_el.classList.remove("fade");
            }, 600);
            break;
        case "ON-OFF-LOOP":
            this.video_el.play();

            if (this.v_index === 1) {
                this.button_el.fadeDescriptionActive(2);
            } else if (this.v_index === 2) {
                this.button_el.fadeDescriptionActive(1);
            } else if (this.v_index === 3) {
                this.button_el.fadeOutSidecopy();
            }

            var _self = this;
            setTimeout(function () {
                // Adding lastframe image to the background container
                _self.video_container_el.style.backgroundImage = "url('../videos/" + _self.lang + "/" + _self.settings.lastframes[_self.v_index];
                _self.video_el.classList.remove("fade");
            }, 600);
            break;
        case "ON-OFF":
            if (btn_feedback) {
                this.video_el.style.opacity = "";
                this.video_el.pause();
                if (this.v_index === 1) {
                    this.video_el.pause();
                    this.v_index = 2;
                    this.button_el.fadeDescriptionActive(2);
                } else if (this.v_index === 3) {
                    this.video_el.pause();
                    this.v_index = 1;
                    this.on_to_off = true;
                    this.button_el.fadeDescriptionActive(1);
                }
                this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];
                this.video_el.classList.add("fade");
            }

            this.video_el.play();
            var _self = this;
            setTimeout(function () {
                // Adding lastframe image to the background container
                _self.video_container_el.style.backgroundImage = "url('../videos/" + _self.lang + "/" + _self.settings.lastframes[_self.v_index];
                _self.video_el.classList.remove("fade");
            }, 600);
            break;
        case "LOOP":
            if (btn_feedback) {
                clearTimeout(this.ctimeOut);
                this.video_el.pause();

                this.video_el.style.opacity = "";

                this.setNextVindex();
                if (this.v_index === 3) {
                    this.button_el.fadeDescriptionActive(2);
                } else if (this.v_index === 5) {
                    this.button_el.fadeDescriptionActive(1);
                }
                this.video_el.classList.remove("fade");
                this.video_el.classList.add("fade");

                this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];
                this.videoLoad = true;
                this.video_el.load();
            }
            else{
                this.video_el.play();
                var _self = this;
                this.ctimeOut = setTimeout(function () {
                    // Adding lastframe image to the background container
                    _self.video_container_el.style.backgroundImage = "url('../videos/" + _self.lang + "/" + _self.settings.lastframes[_self.v_index];
                    _self.video_el.classList.remove("fade");
                }, 600);
            }

            break;
        default:
    }

    if (btn_feedback) {
        this.button_el.toggleCSSAnimation();
    }
}

/**
 * It handles the video playback for the  feature type LOOP
 *
 */
Videoblock.prototype.onVideoEnded_LOOP = function () {
    switch (this.v_index) {
        case 0:
            // AUTOPLAY IN THE DEFAULT MODE
            this.video_el.style.opacity = "";
            this.video_el.classList.add("fade");

            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

            var _self = this;
            setTimeout(function () {
                _self.playVideo();
            }, 500);

            break;
        case 1:
            this.video_el.style.opacity = "";
            this.video_el.classList.add("fade");
            this.setNextVindex();
            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

            this.button_el.button_wrapper.classList.remove("fadedout");
            this.button_el.button_wrapper.classList.add("fadedin");

            var _self = this;
            setTimeout(function () {
                _self.playVideo();
                _self.button_el.enable();
                _self.button_el.rippleEffect();
                _self.sendInteractionAvailable();
            }, 500);

            break;
        case 2:
            this.video_el.style.opacity = "";
            this.video_el.classList.add("fade");
            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

            var _self = this;
            _self.button_el.button_wrapper.classList.remove("fadedout");
            _self.button_el.button_wrapper.classList.add("fadedin");
            setTimeout(function () {
                _self.playVideo();
            }, 500);

            break;
        case 3:
            this.video_el.style.opacity = "";
            this.video_el.classList.add("fade");
            this.setNextVindex();
            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

            this.button_el.button_wrapper.classList.remove("fadedout");
            this.button_el.button_wrapper.classList.add("fadedin");
            var _self = this;
            setTimeout(function () {
                _self.playVideo();
                _self.button_el.enable();
                _self.button_el.rippleEffect();
                _self.sendInteractionAvailable();
            }, 500);
            break;
        case 4:
            this.video_el.style.opacity = "";
            this.video_el.classList.add("fade");
            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

            var _self = this;
            _self.button_el.button_wrapper.classList.remove("fadedout");
            _self.button_el.button_wrapper.classList.add("fadedin");
            setTimeout(function () {
                _self.playVideo();
            }, 500);
            break;
        case 5:
            this.video_el.style.opacity = "";
            this.video_el.classList.add("fade");
            this.v_index = 2;
            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

            var _self = this;
            _self.button_el.button_wrapper.classList.remove("fadedout");
            _self.button_el.button_wrapper.classList.add("fadedin");
            setTimeout(function () {
                _self.playVideo();
                _self.button_el.enable();
                _self.button_el.rippleEffect();
                _self.sendInteractionAvailable();
            }, 500);
            break;
        default:

    }
}

/**
 * It handles the video playback for the feature type BUILD-LOOP-OUT
 *
 */
Videoblock.prototype.onVideoEnded_BLP = function () {
    this.video_el.style.opacity = "";
    this.video_el.classList.add("fade");

    switch (this.v_index) {
        case 0:
            var _self = this;
            if(_self.settings.playFirstTwo){
                _self.v_index = 2;
                _self.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];
                _self.button_el.button_wrapper.classList.remove("fadedout");
                _self.button_el.button_wrapper.classList.add("fadedin");
                setTimeout(function () {
                    _self.enableButton();
                    _self.button_el.rippleEffect();
                }, 500);
                _self.sendInteractionAvailable();
            }
            else {
                _self.v_index = 1;
                _self.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];
                _self.button_el.button_wrapper.classList.remove("fadedout");
                _self.button_el.button_wrapper.classList.add("fadedin");
                setTimeout(function () {
                    _self.enableButton();
                    _self.button_el.rippleEffect();
                }, 500);
                _self.sendInteractionAvailable();
            }

            break;
        case 1:
            this.setNextVindex();
            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

            var _self = this;
            if(_self.settings.playFirstTwo){
                /*
                _self.button_el.button_wrapper.classList.remove("fadedout");
                _self.button_el.button_wrapper.classList.add("fadedin");
                setTimeout(function () {
                    _self.enableButton();
                    _self.button_el.rippleEffect();
                }, 500);
                _self.sendInteractionAvailable();
                */
            }
            else {
                setTimeout(function () {_self.playVideo();}, 500);
            }

            break;
        case 2:
            var event = new Event('[SocketManager]::close_feature');
            window.dispatchEvent(event);

            this.setNextVindex();
            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];
            break;
    }
}

/**
 * It handles the video playback for the feature type ON-OFF-LOOP
 *
 */
Videoblock.prototype.onVideoEnded_ONOFFLOOP = function () {
    if (this.firstplayback) {
        this.firstplayback = false;
        if (this.v_index == 0) {
            this.video_el.style.opacity = "";
            this.video_el.classList.add("fade");
            this.setNextVindex();
            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

            this.button_el.button_wrapper.classList.remove("fadedout");
            this.button_el.button_wrapper.classList.add("fadedin");

            var _self = this;
            setTimeout(function () {
                _self.button_el.rippleEffect();
                _self.enableButton();
                _self.sendInteractionAvailable();
            }, 500);
        }
    } else {
        if (this.v_index === 1) {
            this.video_el.style.opacity = "";
            this.video_el.classList.add("fade");
            this.setNextVindex();
            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

            var _self = this;
            _self.button_el.button_wrapper.classList.remove("fadedout");
            _self.button_el.button_wrapper.classList.add("fadedin");
            setTimeout(function () {
                _self.button_el.rippleEffect();
                _self.enableButton();
                _self.sendInteractionAvailable();
            }, 500);
        } else if (this.v_index === 2) {
            this.video_el.style.opacity = "";
            this.video_el.classList.add("fade");
            this.setNextVindex();
            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

            var _self = this;
            _self.button_el.button_wrapper.classList.remove("fadedout");
            _self.button_el.button_wrapper.classList.add("fadedin");
            setTimeout(function () {
                //_self.playVideo();
                // show copy to restart the thing
                _self.button_el.fadeInSidecopy();
                _self.button_el.rippleEffect();
                _self.enableButton();
                _self.sendInteractionAvailable();
            }, 500);
        } else if (this.v_index === 3) {
            this.button_el.toggleCSSAnimation();

            this.video_el.style.opacity = "";
            this.video_el.classList.add("fade");
            this.setNextVindex();
            this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

            var _self = this;
            _self.button_el.button_wrapper.classList.remove("fadedout");
            _self.button_el.button_wrapper.classList.add("fadedin");
            setTimeout(function () {
                _self.playVideo();
                _self.firstplayback = true;
            }, 500);

        }
    }
}

/**
 * It handles the video playback for the feature type ON-OFF
 *
 */
Videoblock.prototype.onVideoEnded_ONOFF = function () {

    if (this.v_index === 0) {
        this.video_el.style.opacity = "";
        this.video_el.classList.add("fade");
        this.v_index = 1;
        this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

        var _self = this;
        setTimeout(function () {
            _self.button_el.button_wrapper.classList.remove("fadedout");
            _self.button_el.button_wrapper.classList.add("fadedin");
            _self.button_el.enable();
            _self.button_el.rippleEffect();
            _self.sendInteractionAvailable();
            _self.playVideo();
        }, 500);
    } else if (this.v_index === 1) {
        this.video_el.style.opacity = "";
        this.video_el.classList.add("fade");
        this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

        var _self = this;
        setTimeout(function () {
            if (_self.on_to_off) {
                _self.on_to_off = false;
                _self.button_el.button_wrapper.classList.remove("fadedout");
                _self.button_el.button_wrapper.classList.add("fadedin");
                _self.button_el.enable();
                _self.button_el.rippleEffect();
                _self.sendInteractionAvailable();
            }
            _self.playVideo();
        }, 500);
    } else if (this.v_index === 2) {
        this.video_el.style.opacity = "";
        this.video_el.classList.add("fade");
        this.v_index = 3;
        this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

        var _self = this;
        setTimeout(function () {
            _self.button_el.button_wrapper.classList.remove("fadedout");
            _self.button_el.button_wrapper.classList.add("fadedin");
            _self.button_el.enable();
            _self.button_el.rippleEffect();
            _self.sendInteractionAvailable();
            _self.playVideo();
        }, 500);
    } else if (this.v_index === 3) {
        this.video_el.style.opacity = "";
        this.video_el.classList.add("fade");
        //this.v_index = 0;
        this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

        var _self = this;
        setTimeout(function () {
            _self.playVideo();
        }, 500);
    }
}

/**
 * It increases the video index to the next one in the list
 *
 */
Videoblock.prototype.setNextVindex = function () {
    this.v_index = this.v_index + 1;
    if (this.v_index >= this.settings.videos.length) {
        this.v_index = 0;
    }
}

/**
 * Reset playback of the video to the origin state
 *
 */
Videoblock.prototype.resetPlayback = function () {
    this.v_index = this.settings.initIndex;
    this.onCenter = false;

    this.video_container_el.style.backgroundImage = "";
    this.video_el.classList.add("fade");
    this.video_el.src = "../videos/" + this.lang + "/" + this.settings.videos[this.v_index];

    var _self = this;
    setTimeout(function () {
        _self.playVideo();
        // Adding lastframe image to the background container
        _self.video_container_el.style.backgroundImage = "url('../videos/" + _self.lang + "/" + _self.settings.lastframes[_self.v_index];
        _self.video_el.classList.remove("fade");
    }, 600);
}

/**
 * Wrapper function to enable the attached button
 *
 */
Videoblock.prototype.enableButton = function () {
    this.video_el.pause();
    this.button_el.enable();
}

/**
 * Wrapper function to disable the attached button
 *
 */
Videoblock.prototype.disableButton = function () {
    this.button_el.disable();
}

/**
 * It creates a fade in out transition from the default mode to the feature in the center and the other way around
 *
 * @param {object} position settings position of the center.
 * @param {boolean} fadeincenter indicates if the transition starts from the feature mode (center).
 * @param {Centerblock} center dom element of the center.
 */
Videoblock.prototype.transitionToPosition = function (position, fadeincenter, center) {
    var _self = this;
    var el = this.video_container_el;
    var time = 0.5;

    _self.video_el.pause();

    if (!fadeincenter) { // FROM SIDE TO CENTER
        this.on_center = true;
        this.button_el.button_wrapper.classList.remove("fadedin");
        this.button_el.button_wrapper.classList.add("fadedout");
        this.button_el.swapCopy(true);
        this.button_el.fadeOutFeatureName();
    } else { // FROM CENTER TO SIDE
        this.on_center = false;
        this.button_el.fadeOutSidecopy();
        this.button_el.swapCopy(false);
        this.button_el.button_wrapper.classList.remove("fadedout");
        this.button_el.button_wrapper.classList.add("fadedin");
        this.button_el.fadeInFeatureName();
    }

    TweenMax.to(el, time, {
        ease: Power2.easeInOut,
        css: {
            opacity: 0
        },
        onComplete: function () {
            el.style.opacity = 0;
            el.style.top = position.top;
            el.style.left = position.left;
            if (fadeincenter) {
                el.style.zIndex = 400 + _self.settings.button.index;
                // RESET VIDEO (WHEN IS FADED OUT)
                _self.resetVideoAndBackground();
            } else {
                el.style.zIndex = 999;
                if (_self.settings.type === "LOOP") {
                    _self.v_index = 1;
                    _self.video_el.src = "../videos/" + _self.lang + "/" + _self.settings.videos[_self.v_index];
                    _self.video_container_el.style.backgroundImage = "url('../videos/" + _self.lang + "/" + _self.settings.lastframes[_self.v_index];

                    _self.video_container_el.style.width = _self.settings.video.center.width;
                    _self.video_container_el.style.height = _self.settings.video.center.height;
                    _self.video_container_el.style.backgroundSize = _self.settings.video.center.width + " " + _self.settings.video.center.height;

                }
            }
            if (center) {
                center.classList.remove("fadeout");
                center.classList.add("fade");
            }

            TweenMax.to(el, time, {
                ease: Power2.easeInOut,
                css: {
                    opacity: 1.0
                },
                onComplete: function () {
                    // PLAYBACK AFTER TRANSITION
                    switch (_self.settings.type) {
                        case "BUILD-LOOP-OUT":
                            if (!fadeincenter) {
                                //_self.button_el.setBarVisible();
                                _self.playVideo();
                            } else {
                                //_self.button_el.setBarNotVisible();
                                _self.firstplayback = true;
                                _self.sendInteractionAvailable(fadeincenter);
                            }
                            _self.sendTransitonFinished();

                            break;
                        case "ON-OFF-LOOP":
                            if (!fadeincenter) {
                                //_self.button_el.setBarVisible();
                                _self.playVideo();
                            } else {
                                if (_self.button_el.on) {
                                    _self.button_el.toggleCSSAnimation();
                                }
                                //_self.button_el.setBarNotVisible();
                                _self.firstplayback = true;
                                _self.sendInteractionAvailable(fadeincenter);
                            }
                            _self.sendTransitonFinished();
                            break;

                        case "ON-OFF":

                            if (!fadeincenter) {
                                //_self.button_el.setBarVisible();
                                _self.playVideo();
                            } else {
                                if (_self.button_el.on) {
                                    _self.button_el.toggleCSSAnimation();
                                }
                                //_self.button_el.setBarNotVisible();
                                _self.firstplayback = true;
                                _self.sendInteractionAvailable(fadeincenter);
                            }
                            _self.sendTransitonFinished();
                            break;
                        case "LOOP":
                            if (!fadeincenter) {
                                //_self.button_el.setBarVisible();
                                _self.playVideo();
                            } else {
                                if (!_self.button_el.on) {
                                    _self.button_el.toggleCSSAnimation();
                                }
                                //_self.button_el.setBarNotVisible();
                                _self.playVideo();

                                _self.firstplayback = true;
                                _self.sendInteractionAvailable(fadeincenter);
                            }
                            _self.sendTransitonFinished();
                            break;
                        default:
                    }
                }
            });
        }
    });
    // REPLACE COPY ANIMATION
    TweenMax.to(_self.button_el.text_description, time, {
        ease: Power2.easeInOut,
        css: {
            opacity: 0
        },
        onComplete: function () {
            if (fadeincenter) {
                el.style.zIndex = 400 + _self.settings.button.index;
                _self.button_el.setDescription(0);
            } else {
                el.style.zIndex = 999;
                _self.button_el.setDescription(1);
            }

            TweenMax.to(_self.button_el.text_description, time, {
                ease: Power2.easeInOut,
                css: {
                    opacity: 1
                },
            });
        }
    });
}

/**
 * Fade out animation for the block
 *
 */
Videoblock.prototype.fadeDisable = function () {
    var el = this.video_container_el;
    var time = 1.0;
    TweenMax.to(el, time, {
        ease: Power2.easeInOut,
        css: {
            opacity: 0.3
        },
    });

    this.button_el.text_container.classList.remove("enabled");
    this.button_el.text_container.classList.add("disabled");

    this.button_el.button_wrapper.classList.remove("fadedin");
    this.button_el.button_wrapper.classList.add("fadedout");
}

/**
 * Fade in animation for the block
 *
 */
Videoblock.prototype.fadeEnable = function () {
    var el = this.video_container_el;
    var time = 1.0;
    TweenMax.to(el, time, {
        ease: Power2.easeInOut,
        css: {
            opacity: 1.0
        },
    });

    this.button_el.text_container.classList.remove("disabled");
    this.button_el.text_container.classList.add("enabled");

    this.button_el.button_wrapper.classList.remove("fadedout");
    this.button_el.button_wrapper.classList.add("fadedin");
}

/**
 * Sends custom event to let know the app that the buttons interaction is enabled
 *
 */
Videoblock.prototype.sendInteractionAvailable = function (fadeincenter) {
    var event = new Event('[SocketManager]::interaction_available');
    if (fadeincenter) {
        event.fadeInCenter = true;
    }
    window.dispatchEvent(event);
}

/**
 * Sends custom event to let know the app that the transitions is finished
 *
 */
Videoblock.prototype.sendTransitonFinished = function (delay) {
    if (delay) {
        var dly = delay;
    } else {
        var dly = 0;
    }
    var event = new Event('[SocketManager]::transition_finished');
    setTimeout(function () {
        window.dispatchEvent(event);
    }, dly);
}

/**
 * Sets the video element to the init state
 *
 */
Videoblock.prototype.resetVideoAndBackground = function () {
    var _self = this;
    switch (_self.settings.type) {
        case "BUILD-LOOP-OUT":
            _self.v_index = 0;
            _self.video_el.src = "../videos/" + _self.lang + "/" + _self.settings.videos[_self.v_index];
            _self.video_container_el.style.backgroundImage = "url('../videos/" + _self.lang + "/" + _self.settings.lastframes[_self.v_index];

            break;
        case "ON-OFF-LOOP":
            _self.v_index = 0;
            _self.video_el.src = "../videos/" + _self.lang + "/" + _self.settings.videos[_self.v_index];
            _self.video_container_el.style.backgroundImage = "url('../videos/" + _self.lang + "/" + _self.settings.lastframes[_self.v_index];
            break;
        case "ON-OFF":
            _self.v_index = 0;
            _self.video_el.src = "../videos/" + _self.lang + "/" + _self.settings.videos[_self.v_index];
            _self.video_container_el.style.backgroundImage = "url('../videos/" + _self.lang + "/" + _self.settings.lastframes[_self.v_index];
            break;
        case "LOOP":
            _self.v_index = 0;
            _self.video_el.src = "../videos/" + _self.lang + "/" + _self.settings.videos[_self.v_index];
            _self.video_container_el.style.backgroundImage = "url('../videos/" + _self.lang + "/" + _self.settings.lastframes[_self.v_index];

            _self.video_container_el.style.width = _self.settings.video.side.width;
            _self.video_container_el.style.height = _self.settings.video.side.height;
            _self.video_container_el.style.backgroundSize = _self.settings.video.side.width + " " + _self.settings.video.side.height;
            break;
        default:
    }
}
