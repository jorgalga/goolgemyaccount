/** Class representing the close Button of the app. */
module.exports = Closebutton = function (app_dom, settings) {
    this.app_dom = app_dom;
    this.settings = settings;

    this.close_container_el = {};
    this.close_description = {};
    this.close_arrow = {};

    this.createDomElements();
}

/**
 * Creates the DOM elements, it applies the css that is in the settings and it appends to the app element.
 *
 */
Closebutton.prototype.createDomElements = function() {
    var c = document.createComment("CLOSE BLOCK");
    this.app_dom.appendChild(c);

    this.close_container_el = document.createElement("DIV");
    this.close_container_el.setAttribute("id", "close_container");

    this.close_description = document.createElement("DIV");
    this.close_description.classList.add("close-description");
    this.close_description.innerHTML = this.settings.description;

    this.close_arrow = document.createElement("IMG");
    this.close_arrow.setAttribute("src", "../images/arrow.png");
    this.close_arrow.classList.add("close-arrow");

    this.close_container_el.appendChild(this.close_description);
    this.close_container_el.appendChild(this.close_arrow);

    this.app_dom.appendChild(this.close_container_el);

    this.close_container_el.style.top = this.settings.top;
    this.close_container_el.style.left = this.settings.left;
    this.close_container_el.style.width = this.settings.width;
}

/**
 * CSS Visual feedback for the button at the feature mode.
 *
 */
Closebutton.prototype.setEnabled = function(){
    var _self = this;
    setTimeout(function(){
        _self.close_description.innerHTML = _self.settings.description_active;
        _self.close_description.classList.add("adjusted");
    }, 500);

    this.close_container_el.classList.remove("disabled");
    this.close_container_el.classList.add("enabled");

    this.close_arrow.classList.remove("visible");
    this.close_arrow.classList.add("hidden");
}

/**
 * CSS Visual feedback for the button at the default mode.
 *
 */
Closebutton.prototype.setDisabled = function(){
    var _self = this;
    setTimeout(function(){
        _self.close_description.classList.remove("adjusted");
        _self.close_description.innerHTML = _self.settings.description;
    }, 500);
    this.close_container_el.classList.remove("enabled");
    this.close_container_el.classList.add("disabled");

    this.close_arrow.classList.remove("hidden");
    this.close_arrow.classList.add("visible");
}

/**
 * Tween fadeout animation durint a transition while the button is inactive.
 *
 */
Closebutton.prototype.setFadeDisabled = function(){
    var el = this.close_container_el;
    TweenMax.to(el, 0.5, {
        ease: Power2.easeInOut,
        css:{opacity:0.3}
    });
}

/**
 * Tween fadein animation to let know the user that the button is active again.
 *
 */
Closebutton.prototype.setFadeEnabled = function(){
    var el = this.close_container_el;
    TweenMax.to(el, 0.5, {
        ease: Power2.easeInOut,
        css:{opacity:1}
    });
}
