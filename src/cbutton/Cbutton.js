module.exports = Cbutton = function (parent, settings) {
    this.parent_el = parent;
    this.settings = settings;

    this.button_wrapper = {};
    this.container = {};
    this.circle = {};
    this.ripple = {};

    this.text_container = {};
    this.text_feature = {};
    this.text_sidecopy = {};
    this.video_bar = {};
    this.video_progress = {};
    this.text_description = {};

    this.on = false;

    this.createDomElements();
    //this.setupResizeListener();
}

Cbutton.prototype.createDomElements = function() {
    var c = document.createComment("BLOCK");
    this.parent_el.appendChild(c);

    // Creating  DOM for button elements
    this.button_wrapper = document.createElement("DIV");
    this.button_wrapper.setAttribute("id", "button_wrapper_" + this.settings.button.index);
    this.button_wrapper.classList.add("button-wrapper");

    this.container = document.createElement("DIV");
    this.container.setAttribute("id", "button-container-" + this.settings.button.index);
    this.container.classList.add("button-container");
    this.container.classList.add(this.settings.status);

    if(!this.settings.button.label) {
        this.circle = document.createElement("DIV");
        this.circle.setAttribute("id", "circle-" + this.settings.button.index);
        this.circle.classList.add("circle-object");
        this.container.appendChild(this.circle);
    }
    else{
        this.button_wrapper.classList.add("label");
        this.container.innerHTML = this.settings.button.label;
    }

    //<div class="ripple"><img src="images/ripple.png" /></div>
    this.ripple = document.createElement("DIV");
    this.ripple.setAttribute("id", "ripple-container-" + this.settings.button.index);
    this.ripple.classList.add("ripple-container");

    // Creating DOM for text elements
    this.text_container = document.createElement("DIV");
    this.text_container.setAttribute("id", "text_button_container_" + this.settings.button.index);
    this.text_container.classList.add("text-button-container");

    this.text_feature = document.createElement("DIV");
    this.text_feature.classList.add("feature");
    this.text_feature.innerHTML = this.settings.copy.feature;

    this.video_bar = document.createElement("DIV");
    this.video_bar.classList.add("video-bar");
    this.video_progress = document.createElement("DIV");
    this.video_progress.classList.add("video_progress");
    this.video_bar.appendChild(this.video_progress);

    this.text_description = document.createElement("DIV");
    this.text_description.classList.add("description");
    this.text_description.innerHTML = this.settings.copy.description_active[0];

    if(this.settings.copy.sidecopy){
        this.text_sidecopy = document.createElement("DIV");
        this.text_sidecopy.setAttribute("id", "side-copy-" + this.settings.button.index);
        this.text_sidecopy.innerHTML = this.settings.copy.sidecopy;
        this.text_container.appendChild(this.text_sidecopy);
    }

    // Apply styles
    this.text_container.style.top = this.settings.copy.top;
    this.text_container.style.left = this.settings.copy.left;
    this.text_container.style.zIndex = 1100 + this.settings.button.index;
    this.text_container.style.width = this.settings.copy.width;

    this.button_wrapper.style.top = this.settings.button.top;
    this.button_wrapper.style.left = this.settings.button.left;
    this.button_wrapper.style.zIndex = 1000 + this.settings.button.index;

    // Add to DOM
    this.text_container.appendChild(this.text_feature);
    this.text_container.appendChild(this.video_bar);
    this.text_container.appendChild(this.text_description);


    this.parent_el.appendChild(this.text_container);
    this.parent_el.appendChild(this.button_wrapper);

    this.button_wrapper.appendChild(this.container);
    this.button_wrapper.appendChild(this.ripple);

    if(this.settings.status === "enabled") {
        this.toggleCSSAnimation();
    }
};
Cbutton.prototype.reScaleToggle = function(event) {
    this.button_wrapper.style.transform = "scale("+ this.settings.button.scale + ")";
}

Cbutton.prototype.rePositioningToggle = function(event) {
  var _self = this;
  setTimeout(function(){
      var w = _self.text_feature.clientWidth;
      var h = _self.text_feature.clientHeight;
      var el_offset = getOffset(_self.text_feature);

      var t_ofset = 0;
      var l_offset = 0;
      if(_self.settings.button.label){
          t_ofset = 2.5;
          l_offset = 1;
      }

      var top =  (100 * (el_offset.top / screen.height) + t_ofset ) + "%";
      var left = (100 * (el_offset.left / screen.width) + l_offset ) + "%";

      _self.button_wrapper.style.top = top;
      _self.button_wrapper.style.left = left;

  }, 1000);

  function getOffset(el) {
      var rect = el.getBoundingClientRect(),
      scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
      scrollTop = window.pageYOffset || document.documentElement.scrollTop;
      return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
  }
}

Cbutton.prototype.setupResizeListener = function(){
    var _self = this;
    var rtime;
    var timeout = false;
    var delta = 200;

    window.addEventListener("resize", function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeend, delta);
        }
    });

    function resizeend() {
        if (new Date() - rtime < delta) {
            setTimeout(resizeend, delta);
        } else {
            timeout = false;
            _self.rePositioningToggle();
        }
    }
}

Cbutton.prototype.enable = function() {
    var el = this.container;
    TweenMax.to(el, 1, {
        ease: Power2.easeInOut,
        css:{opacity:1}
    });
}

Cbutton.prototype.disable = function() {
    var el = this.container;
    TweenMax.to(el, 1, {
        delay: 0.5 ,
        ease: Power2.easeInOut,
        css:{opacity:0.3}
    });
}
Cbutton.prototype.toggleCSSAnimation = function() {
    if(!this.settings.button.label) {
        var circle = document.getElementById("circle-" + this.settings.button.index);
        var time = this.settings.button.time;

        if(this.on){
            this.container.classList.remove("switched-on");
            this.container.classList.add("switched-off");
            TweenMax.to(circle, time, {
              css:{left:"-5px"}
            });
        }
        else {
            this.container.classList.remove("switched-off");
            this.container.classList.add("switched-on");
            TweenMax.to(circle, time, {
              css:{left:"110px"}
            });
        }
        this.disable();
        this.on = !this.on;
    }
    this.rippleEffectOff();
}

Cbutton.prototype.setDescription = function(index) {
    this.text_description.innerHTML = this.settings.copy.description_active[index];
}

Cbutton.prototype.setProgress = function(progress){
    this.video_progress.style.width = progress + "%";
}

Cbutton.prototype.setBarVisible = function(){
    this.video_bar.classList.remove("disabled");
    this.video_bar.classList.add("enabled");
}

Cbutton.prototype.setBarNotVisible = function(){
    this.video_bar.classList.remove("enabled");
    this.video_bar.classList.add("disabled");
}

Cbutton.prototype.rippleEffect = function(){
    var el = this.ripple;
    var time = 0.5;
    var scale = 1.0;
    if(this.settings.button.label) scale = 0.6;
    el.style.opacity = 1;
    TweenMax.to(el, time*2, {
        ease: Elastic.easeOut.config(1, 0.3),
        css:{scaleX:scale, scaleY:scale}
    });
}
Cbutton.prototype.rippleEffectOff = function(){
    var el = this.ripple;
    var _self = this;

    TweenMax.to(el, 0.5, {
        ease: Power2.easeInOut,
        css:{opacity:0},
        onComplete: function(){

        }
    });
    TweenMax.to(el, 0.5, {
        delay: 0.5,
        ease: Power2.easeInOut,
        css:{scaleX:0.0, scaleY:0.0}
    });

}

Cbutton.prototype.swapCopy = function(active){
    var _self = this;
    var el = this.container;
    var text = "";
    if(this.settings.button.label_active){
        if(active){
            text = this.settings.button.label_active;
        }
        else {
            text = this.settings.button.label;
        }

        TweenMax.to(el, 0.5, {
        ease: Power2.easeInOut,
        css:{opacity:0},
        onComplete: function(){
            _self.container.innerHTML = text;
            TweenMax.to(el, 0.5, {
                ease: Power2.easeInOut,
                css:{opacity:1}
            });
            }
        });
    }
}

Cbutton.prototype.fadeDescriptionActive = function(index){
    var el = this.text_description;
    var _self = this;
    TweenMax.to(el, 0.3, {
        ease: Power2.easeInOut,
        css:{opacity:0},
        onComplete: function(){
          _self.setDescription(index);
          TweenMax.to(el, 0.3, {
              ease: Power2.easeInOut,
              css:{opacity:1}
          });
        }
    });
}

Cbutton.prototype.fadeOutFeatureName = function(){
    var el = this.text_feature;
    TweenMax.to(el, 1, {
        ease: Power2.easeInOut,
        css:{opacity:0}
    });
}
Cbutton.prototype.fadeInFeatureName = function(){
    var el = this.text_feature;
    TweenMax.to(el, 1, {
        ease: Power2.easeInOut,
        css:{opacity:1}
    });
}

Cbutton.prototype.fadeOutSidecopy = function(){
    if(this.text_sidecopy) {
        var el = this.text_sidecopy;
        TweenMax.to(el, 1, {
            ease: Power2.easeInOut,
            css:{opacity:0}
        });
    }
}
Cbutton.prototype.fadeInSidecopy = function(){
    if(this.text_sidecopy) {
        var el = this.text_sidecopy;
        TweenMax.to(el, 1, {
            ease: Power2.easeInOut,
            css:{opacity:1}
        });
    }
}
