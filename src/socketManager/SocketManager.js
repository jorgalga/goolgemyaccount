var SMOTHIE = require("../../node_modules/smoothie/smoothie.js");

module.exports = SocketManager = function (test, socket, videoblocks, center, close, inputs) {
    this.test = test;
    this.socket = socket;
    this.videoblocks = videoblocks;
    this.center = center;
    this.close = close;

    this.transition = false;
    this.available = true;
    this.active = -1;

    this.c_timeout_init = 30000;
    this.c_timeout_flow = 30000;
    this.timeoutVar = {};

    this.def = [];
    this.multi = [];

    for ( var key in inputs){
        if (inputs.hasOwnProperty(key)) {
            this.def.push(inputs[key].def);
            this.multi.push(inputs[key].multi);
        }
    }

    this.setupSocketEvents();
    this.setupCustomEvents();
    this.setupKeyboardEvents();


    var canvas = document.createElement("CANVAS");

    canvas.setAttribute("id", "mycanvas");
    canvas.setAttribute("width", "500px");
    canvas.setAttribute("height", "100px");
    document.body.appendChild(canvas);

    this.line0 = new SMOTHIE.TimeSeries();
    this.line1 = new SMOTHIE.TimeSeries();
    this.line2 = new SMOTHIE.TimeSeries();
    this.line3 = new SMOTHIE.TimeSeries();
    this.line4 = new SMOTHIE.TimeSeries();
    this.line5 = new SMOTHIE.TimeSeries();
    this.line6 = new SMOTHIE.TimeSeries();
    this.setupSmothie();
}

SocketManager.prototype.setupSmothie = function() {
    var smoo = new SMOTHIE.SmoothieChart({
      maxValue: 1.0,
      minValue: 0.0,
      grid: { strokeStyle:'rgb(125, 0, 0)', fillStyle:'rgb(60, 0, 0)',
              lineWidth: 1, millisPerLine: 250, verticalSections: 6, },
      labels: { fillStyle:'rgb(255, 255, 255)' }
    });
    smoo.streamTo(document.getElementById("mycanvas"));

    // Add to SmoothieChart
    smoo.addTimeSeries(this.line0,
        { strokeStyle:'rgb(240, 0, 240)',   fillStyle:'rgba(240, 0, 240, 0.3)', lineWidth:2 });
    smoo.addTimeSeries(this.line1,
        { strokeStyle:'rgb(230, 0, 230)', fillStyle:'rgba(230, 0, 230, 0.3)', lineWidth:2 });
    smoo.addTimeSeries(this.line2,
        { strokeStyle:'rgb(220, 0, 220)', fillStyle:'rgba(220, 0, 220, 0.3)', lineWidth:2 });
    smoo.addTimeSeries(this.line3,
        { strokeStyle:'rgb(210, 0, 210)', fillStyle:'rgba(210, 0, 210, 0.3)', lineWidth:2 });
    smoo.addTimeSeries(this.line4,
        { strokeStyle:'rgb(200, 0, 200)', fillStyle:'rgba(200, 0, 200, 0.3)', lineWidth:2 });
    smoo.addTimeSeries(this.line5,
        { strokeStyle:'rgb(190, 0, 190)', fillStyle:'rgba(190, 0, 190, 0.3)', lineWidth:2 });
    smoo.addTimeSeries(this.line6,
        { strokeStyle:'rgb(0, 255, 0)', fillStyle:'rgba(0, 255, 0, 0.3)', lineWidth:2 });

}

SocketManager.prototype.setupSocketEvents = function() {
    var _self = this;


    _self.socket.on('input_values', function (data){
        // FILTERING
        data[0] = Math.max(0, (data[0]-_self.def[0])/255) * _self.multi[0];
        data[1] = Math.max(0, (data[1]-_self.def[1])/255) * _self.multi[1];
        data[2] = Math.max(0, (data[2]-_self.def[2])/255) * _self.multi[2];
        data[3] = Math.max(0, (data[3]-_self.def[3])/255) * _self.multi[3];
        data[4] = Math.max(0, (data[4]-_self.def[4])/255) * _self.multi[4];
        data[5] = Math.max(0, (data[5]-_self.def[5])/255) * _self.multi[5];
        data[6] = Math.max(0, (data[6]-_self.def[6])/255) * _self.multi[6];

        if(true){
            // Add the values to the chart every 'input_values' event from the socket
            _self.line0.append(new Date().getTime(), data[0] );
            _self.line1.append(new Date().getTime(), data[1] );
            _self.line2.append(new Date().getTime(), data[2] );
            _self.line3.append(new Date().getTime(), data[3] );
            _self.line4.append(new Date().getTime(), data[4] );
            _self.line5.append(new Date().getTime(), data[5] );
            _self.line6.append(new Date().getTime(), data[6] );
        }

        // CHECK threshold if the manager is available
        //_self.available = false;
        if(!_self.transition){
            checkButtons();
        }

        function checkButtons(){
            for (i = 0; i < data.length; i++) {
                if(data[i] > 0.4) {
                    _self.appNavigationState(i);
                }
            }
        }
    });
}

SocketManager.prototype.setupCustomEvents = function() {
    var _self = this;

    window.addEventListener('[SocketManager]::interaction_available', function (e) {
        _self.available = true;
    }, false);

    window.addEventListener('[SocketManager]::transition_finished', function (e) {
        _self.transition = false;
        _self.close.setFadeEnabled();
    }, false);

    window.addEventListener('[SocketManager]::close_feature', function (e) {
        _self.goToDefaultMode();
    }, false);

}

SocketManager.prototype.setupKeyboardEvents =  function() {
    var _self = this;
    document.addEventListener("keydown", function(e) {
        if(e.keyCode >= 49 && e.keyCode <= 55) {
            var btn = e.keyCode - 49;

            if(!_self.transition) {
                _self.appNavigationState(btn);
            }
        }
    }, false);
}

/**
 * It handels the interaction that comes from the arduino board setting up the several aplication modes.
 *
 */
SocketManager.prototype.appNavigationState = function(btn) {
    var _self = this;

    if(btn === 6 && _self.active !== -1){
        //  Reset
        _self.transition = true;
        _self.goToDefaultMode();
        clearTimeout( _self.timeoutVar);
    }
    else{
        if(_self.available) {
            if(_self.active === -1) {
                if(btn !== 6) {
                    _self.available = false;
                    // Go to Center
                    _self.transition = true;
                    _self.goToFeatureMode(btn);

                    if(!_self.test){
                        clearTimeout( _self.timeoutVar);
                        _self.timeoutVar = setTimeout(function(){
                            _self.goToDefaultMode();
                        }, _self.c_timeout_init);
                    }
                }
            }
            else {
                if(btn !== 6) {
                    if (_self.active === btn) {
                        _self.available = false;
                        _self.videoblocks[_self.active].playVideo(true);

                        if(!_self.test){
                            clearTimeout( _self.timeoutVar);
                            _self.timeoutVar = setTimeout(function(){
                                _self.goToDefaultMode();
                            }, _self.c_timeout_flow);
                        }
                    }
                }
            }
        }
    }
}

SocketManager.prototype.goToFeatureMode = function(btn) {
    this.close.setEnabled();
    this.close.setFadeDisabled();
    this.active = btn;
    this.center.center_container_el.classList.remove("fade");
    this.center.center_container_el.classList.add("fadeout");

    for(i = 0; i < this.videoblocks.length; i++) {
        if(this.active === i){
            this.videoblocks[i].transitionToPosition(this.videoblocks[i].settings.video.center, false);
        }
        else{
            this.videoblocks[i].fadeDisable();
        }
    }
}

SocketManager.prototype.goToDefaultMode = function() {
    this.close.setDisabled();
    for(i = 0; i < this.videoblocks.length; i++) {
        if(this.active === i){
            this.videoblocks[i].on_center = false;
            this.videoblocks[i].disableButton();
            this.videoblocks[i].button_el.rippleEffectOff();
            var p = {}
            if(this.videoblocks[this.active].settings.video.side){
                p = this.videoblocks[this.active].settings.video.side;
            }
            else{
                p.top = "0%";
                p.left = "100%";
                p.width = "20%";
            }
            this.videoblocks[i].transitionToPosition(p, true, this.center.center_container_el);
        }
        else{
            this.videoblocks[i].fadeEnable();
        }
    }
    this.active = -1;
}
