module.exports = Centerblock = function (app_dom, settings) {
    this.app_dom = app_dom;
    this.settings = settings;

    this.center_container_el = {};
    this.center_logo = {};
    this.center_title = {};
    this.center_description = {};

    this.createDomElements();

    var _self = this;
}

Centerblock.prototype.createDomElements = function() {
    var c = document.createComment("CENTER BLOCK");
    this.app_dom.appendChild(c);

    this.center_container_el = document.createElement("DIV");
    this.center_container_el.setAttribute("id", "center_container");

    this.center_logo = document.createElement("DIV");
    this.center_logo.classList.add("center-logo");
    var img = document.createElement("IMG");
    img.setAttribute("src", "../images/" + this.settings.logo);
    this.center_logo.appendChild(img);

    this.center_title = document.createElement("DIV");
    this.center_title.classList.add("center-title");
    this.center_title.innerHTML = this.settings.title;

    this.center_description = document.createElement("DIV");
    this.center_description.classList.add("center-description");
    this.center_description.innerHTML = this.settings.description;

    this.center_container_el.appendChild(this.center_logo);
    this.center_container_el.appendChild(this.center_title);
    this.center_container_el.appendChild(this.center_description);

    this.app_dom.appendChild(this.center_container_el);

    this.setStyle();
}

Centerblock.prototype.updateSetttings = function(settings) {
    this.settings.top = settings.top;
    this.settings.left = settings.left;
    this.settings.width = settings.width;
    this.settings.scale = settings.scale;

    this.setStyle();
}

Centerblock.prototype.setStyle = function() {
    this.center_container_el.style.top = this.settings.top;
    this.center_container_el.style.left = this.settings.left;
    this.center_container_el.style.width = this.settings.width;
    this.center_container_el.style.transform = "scale("+this.settings.scale+")";
}
