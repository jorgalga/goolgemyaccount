module.exports = ScreenManager = function (element) {
    this.fullscreen = true;
    this.element = element// KEYBOARD Listener

    var _self = this;
    document.addEventListener("keydown", function(e) {
        if (e.keyCode == 13) {    // ENTER key
            _self.toggle();
        }
    }, false);
};

ScreenManager.prototype.isFullscreen = function () {
    return this.fullscreen;
};

ScreenManager.prototype.toggle = function () {
    if(this.fullscreen){
        this.fullscreen = false;
        exitFullscreen();
    }
    else{
        this.fullscreen = true;
        launchIntoFullscreen(this.element);
    }

    function launchIntoFullscreen(element) {
        if(element.requestFullscreen) {
          element.requestFullscreen();
        }
        else if(element.mozRequestFullScreen) {
          element.mozRequestFullScreen();
        }
        else if(element.webkitRequestFullscreen) {
          element.webkitRequestFullscreen();
        }
        else if(element.msRequestFullscreen) {
          element.msRequestFullscreen();
        }
    }

    function exitFullscreen() {
        if(document.exitFullscreen) {
          document.exitFullscreen();
        }
        else if(document.mozCancelFullScreen) {
          document.mozCancelFullScreen();
        }
        else if(document.webkitExitFullscreen) {
          document.webkitExitFullscreen();
        }
    }
};
