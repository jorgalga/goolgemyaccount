// NODE app

//Global vars
var server = {};
var io = {};
var exec = require('child_process').exec;
var os = require('os').platform();

// LOCKING FOR ARDUINO DEVICE
exec('gort scan serial' , function(err, stdout, stderr) {
    if(err){
    }
    else{
        console.log("system: " + os + "\n" + stdout);

        //Init functions
	init_express();
        init_socket();
	

        var device = getArduinoName(stdout, os);
        if(device){
            console.log("STARTS WITH ARDUINO");
            init_arduino(device);
        }
        else {
            console.log("STARTS WITHOUT ARDUINO");
        }
    }
});
/**
 * Gets the arduino device in case it is conected to the Serial port. It takes care of the OS odd (mac or linux).
 *
 */
function getArduinoName(stdout, os){
    stdout = stdout.split("\n");

    // MAC OS
    if (os === 'darwin'){
        var pattern = '/dev/tty.usbmodem';
    } // LINUX OS
    else if (os === 'linux'){
        var pattern = '/dev/ttyACM';
    }

    for(var i=0; i < stdout.length-1; i++){
        if(stdout[i].indexOf(pattern) !== -1){
            return stdout[i].split(" ")[1].replace("[", "").replace("]","");
        }
    }
    return;
}

// FUNCTIONS
function init_express(){
    var express = require('express');
    var path = require('path');
    var app = express();
    app.set('port', 9000);
    app.use(express.static(path.join(__dirname, '/public/')));
    // Listen for requests
    server = app.listen(app.get('port'), function() {
        var port = server.address().port;
        console.log('Server listens on port: ' + port);

        exec('google-chrome --incognito --start-fullscreen http://localhost:9000/'+process.argv[2]+'/' , function(err) {
            if(err){ //process error
            }
            else{
                console.log("Chrome launched");
            }
        });
    });
}

function init_socket() {
    io = require('socket.io')(server);
    io.on('connection', function(socket) {
        console.log("connection");
        socket.on('savefile', function (data) {
            var util = require('util');
            var fs = require('fs');
            var jsonData = "var settings = " + util.inspect(data, false, null);
            fs.writeFile("public/js/settings_"+data.lang+".js", jsonData, function(err) {
                if(err) {
                    return console.log(err);
                }
            });

        });
    });
}

function init_arduino(device) {
    var SerialPort = require("serialport");
    var serialport = new SerialPort(device, {
        baudRate: 9600,
        parser: SerialPort.parsers.readline('\n')
    });
    var n_inputs = 7;
    var input_values = [];
    serialport.on('open', function(){
        console.log('Serial Port Opend');
        serialport.on('data', function(data){
            var event = getEventData(data);
            if(event.button === n_inputs - 1){
              input_values.push(event.value);
              io.emit("input_values", input_values);
              input_values = [];
            }
            else {
              input_values.push(event.value );
            }
        });
    });
}

// Utils
function getEventData(data){
    var button = Math.trunc(data / 1000);
    var value = data - button * 1000;

    return {button: button - 2, value: value};
}
