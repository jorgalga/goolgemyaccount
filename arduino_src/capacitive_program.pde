// Pin for the LED
int LEDPin = 13;
// Pin to connect to your drawing
int capSensePin2 = 2;
int capSensePin3 = 3;
int capSensePin4 = 4;
int capSensePin5 = 5;
int capSensePin6 = 6;
int capSensePin7 = 7;
int capSensePin8 = 8;
int pint2Multi = 2000;
int pint3Multi = 3000;
int pint4Multi = 4000;
int pint5Multi = 5000;
int pint6Multi = 6000;
int pint7Multi = 7000;
int pint8Multi = 8000;
int value2;
int value3;
int value4;
int value5;
int value6;
int value7;
int value8;

// This is how high the sensor needs to read in order
//  to trigger a touch.  You'll find this number
//  by trial and error, or you could take readings at
//  the start of the program to dynamically calculate this.
int touchedCutoff = 60;
void setup(){
  Serial.begin(9600);
  // Set up the LED
  pinMode(LEDPin, OUTPUT);
  digitalWrite(LEDPin, LOW);
}
void loop(){
  // If the capacitive sensor reads above a certain threshold,
  //  turn on the LED
  if (readCapacitivePin(capSensePin2) > touchedCutoff) {
    digitalWrite(LEDPin, HIGH);
  }
  else {
    digitalWrite(LEDPin, LOW);
  }

  // Every 500 ms, print the value of the capacitive sensor
  if ( (millis() % 50) == 0){
    value2 = readCapacitivePin(capSensePin2);
    value3 = readCapacitivePin(capSensePin3);
    value4 = readCapacitivePin(capSensePin4);
    value5 = readCapacitivePin(capSensePin5);
    value6 = readCapacitivePin(capSensePin6);
    value7 = readCapacitivePin(capSensePin7);
    value8 = readCapacitivePin(capSensePin8);
    //Serial.flush();
    Serial.println(value2 + pint2Multi);
    Serial.println(value3 + pint3Multi);
    Serial.println(value4 + pint4Multi);
    Serial.println(value5 + pint5Multi);
    Serial.println(value6 + pint6Multi);
    Serial.println(value7 + pint7Multi);
    Serial.println(value8 + pint8Multi);
  }
}

void writeIntAsBinary(int pin, int value){
    Serial.write(-1 * pin);
    Serial.write(value);
}

// readCapacitivePin
//  Input: Arduino pin number
//  Output: A number, from 0 to 17 expressing
//          how much capacitance is on the pin
//  When you touch the pin, or whatever you have
//  attached to it, the number will get higher
//  In order for this to work now,
// The pin should have a 1+Megaohm resistor pulling
//  it up to +5v.
uint8_t readCapacitivePin(int pinToMeasure){
  // This is how you declare a variable which
  //  will hold the PORT, PIN, and DDR registers
  //  on an AVR
  volatile uint8_t* port;
  volatile uint8_t* ddr;
  volatile uint8_t* pin;
  // Here we translate the input pin number from
  //  Arduino pin number to the AVR PORT, PIN, DDR,
  //  and which bit of those registers we care about.
  byte bitmask;
  if ((pinToMeasure >= 0) && (pinToMeasure <= 7)){
    port = &PORTD;
    ddr = &DDRD;
    bitmask = 1 << pinToMeasure;
    pin = &PIND;
  }
  if ((pinToMeasure > 7) && (pinToMeasure <= 13)){
    port = &PORTB;
    ddr = &DDRB;
    bitmask = 1 << (pinToMeasure - 8);
    pin = &PINB;
  }
  if ((pinToMeasure > 13) && (pinToMeasure <= 19)){
    port = &PORTC;
    ddr = &DDRC;
    bitmask = 1 << (pinToMeasure - 13);
    pin = &PINC;
  }
  // Discharge the pin first by setting it low and output
  *port &= ~(bitmask);
  *ddr  |= bitmask;
  delay(1);
  // Make the pin an input WITHOUT the internal pull-up on
  *ddr &= ~(bitmask);
  // Now see how long the pin to get pulled up
  int cycles = 16000;
  for(int i = 0; i < cycles; i++){
    if (*pin & bitmask){
      cycles = i;
      break;
    }
  }
  // Discharge the pin again by setting it low and output
  //  It's important to leave the pins low if you want to
  //  be able to touch more than 1 sensor at a time - if
  //  the sensor is left pulled high, when you touch
  //  two sensors, your body will transfer the charge between
  //  sensors.
  *port &= ~(bitmask);
  *ddr  |= bitmask;

  return cycles;
}
