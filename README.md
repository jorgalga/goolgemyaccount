## Synopsis

The app is designed to run several videos displayed on the DOM which are triggered by a serie of capactive buttons connected to an arduino board.

[Video Making of](https://www.youtube.com/watch?v=_Jor3TRXniw)

## Installation of the dependecies
- Install [Gort](http://gort.io/documentation/getting_started/downloads/)
- npm install -g browserify
- In mac you may need this command: PATH=$PATH:~/.npm-global/bin/
- Run 'npm install'

## Compile the app and start the server with english
- Run 'npm run bundle-start-en'

## Compile the app and start the server with german
- Run 'npm run bundle-start-de'

## Just compile the app
- Run 'npm run bundle'

## Starting the app in German
- Run 'npm run start-de'

## Starting the app in English
- Run 'npm run start-en'